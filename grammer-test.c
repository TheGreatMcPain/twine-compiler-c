#include <first_follow_sets.h>
#include <grammar.h>
#include <stdio.h>
#include <string.h>
#include <table_generator.h>

int main() {
  printf("GRAMMAR--------\n");
  print_grammar_as_enums();

  printf("FIRST_SETS-----\n");
  print_first_sets();

  printf("FOLLOW_SETS----\n");
  print_follow_sets();

  printf("PARSE_TABLE----\n");
  print_parse_table();
}
