#include <ctype.h>
#include <scanner.h>
#include <scanner_utils.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <token_twine.h>
#include <twine_enum.h>
#include <twine_errors.h>
#include <utils.h>

// STATIC GLOBAL Variables used by next()
static int next_line_counter = 0;
static int next_char_counter = -1; // Makes char counter start at 0
static off_t next_prev_position = 0;
static char next_filename[1024];
static bool next_filename_set = false;

// Used for filename. (Must run before next())
void scanner_set_filename(const char *filename) {
  if (!next_filename_set) {
    strcpy(next_filename, get_basename((char *)filename));
    next_filename_set = true;
  }
}

// Get next token without advancing pointer.
struct Token *peek(FILE *input_program) {
  // Store current filepointer.
  off_t current_file_position = ftello(input_program);

  // Call next().
  struct Token *next_token = next(input_program);

  // Seek filepointer to saved location.
  fseeko(input_program, current_file_position, SEEK_SET);

  return next_token;
}

// Return an array of Token pointers.
int scan(FILE *input_program, struct Token ***results) {
  // We don't want to mess with an array that's not empty.
  if (*results != NULL) {
    return -1;
  }

  // Initial array setup.
  int array_size = 1;
  struct Token **tokens = (struct Token **)malloc(sizeof(struct Token *));
  tokens[array_size - 1] = next(input_program);

  // If the first next() call fails free the tokens array.
  if (!tokens[array_size - 1]) {
    free(tokens);
    return -1;
  }

  // Append new tokens as we scan the file.
  while (tokens[array_size - 1]->token_type != token_type_end_of_file) {
    array_size++;
    tokens =
        (struct Token **)realloc(tokens, sizeof(struct Token *) * array_size);
    tokens[array_size - 1] = next(input_program);

    // If next() fails free all tokens and the tokens array.
    if (!tokens[array_size - 1]) {
      for (int i = 0; i < array_size; i++) {
        if (tokens[i]) {
          free_token(tokens[i]);
        }
      }
      free(tokens);
      return -1;
    }
  }

  // Assign the results pointer to the new tokens array.
  *results = tokens;

  // Return the size of the array.
  return array_size;
}

// A bit un-optimized, but this will grab the current line
// of a file pointer.
void get_current_line(FILE *file_stream, char current_line[]) {
  off_t next_newline_pos;
  off_t prev_newline_pos;
  off_t old_position = ftello(file_stream);
  char c = 0;

  size_t current_line_size = 0;

  // Move pointer to next new line.
  move_to_newline(file_stream);
  next_newline_pos = ftello(file_stream);

  // Move pointer to previous new line.
  while (c != '\n') {
    if (ftello(file_stream) == 1) {
      fseeko(file_stream, -1, SEEK_CUR);
      break;
    }
    fseeko(file_stream, -2, SEEK_CUR);
    fread(&c, 1, 1, file_stream);
  }
  prev_newline_pos = ftello(file_stream);

  // Initialize 'current_line' to size of line.
  current_line_size = next_newline_pos - prev_newline_pos;

  // Read the contents of the current line into 'current_line'.
  fread(current_line, sizeof(char), current_line_size, file_stream);

  if (current_line[current_line_size - 1] == '\n') {
    current_line_size--;
  }

  current_line[current_line_size] = '\0';

  // Reset file_stream position.
  fseeko(file_stream, old_position, SEEK_SET);
}

// Get next token and advance pointer.
struct Token *next(FILE *input_program) {
  struct Token *token = NULL;
  char error_msg[1024] = {'\0'};
  char accum[300] = {'\0'};
  char current_line[4096] = {'\0'};
  char last_char[2] = {'\0', '\0'};
  int state = state_looking;

  // Stores line_counter, and char_counter during looking state.
  int line_no = 0;
  int char_no = 0;

  // Make sure 'next_filename' is initialized.
  if (!next_filename_set) {
    strcpy(next_filename, "unknown");
  }

  // Read a single character from the file.
  fread(last_char, 1, 1, input_program);

  while (NULL == token) {
    // Check if position is different from previous next() call.
    if (next_prev_position != ftello(input_program)) {
      next_char_counter++;
      if (last_char[0] == '\n') {
        next_line_counter++;
        next_char_counter = -1;
      }

      next_prev_position = ftello(input_program);
    }

    if (state == state_looking) {
      // End of file
      if (feof(input_program) != 0) {
        token = create_token(token_type_end_of_file, "$", -1, -1, "EOF");
        break;
      }

      // White space
      if (isspace(last_char[0])) {
        state = state_looking;
      } else if (last_char[0] == '%') {
        // Comments
        last_char[0] = move_to_newline(input_program);
        next_char_counter = -1;
        next_line_counter++;
        state = state_looking;
      } else if (strncmp(last_char, "0", 1) == 0) {
        // Zero
        state = state_zero;
        strcpy(accum, last_char);
      } else if (isdigit(last_char[0]) && !(strncmp(last_char, "0", 1) == 0)) {
        // Integer
        state = state_integer;
        strcpy(accum, last_char);
      } else if (is_punct(last_char[0])) {
        // Punctuation
        state = get_punct_state(last_char[0]);
        strcpy(accum, last_char);
      } else if (is_operation(last_char[0])) {
        // Operators
        state = get_operation_state(last_char[0]);
        strcpy(accum, last_char);
      } else if (isalpha(last_char[0]) || last_char[0] == '$') {
        // Identifiers and Reserved
        state = state_ids_and_reserved;
        strcpy(accum, last_char);
      } else {
        get_current_line(input_program, current_line);
        sprintf(error_msg, "Invalid character at beginning of identifier: %s",
                last_char);
        raise_LexicalError(next_filename, next_line_counter, next_char_counter,
                           current_line, state_looking, error_msg);
        return NULL;
      }

      line_no = next_line_counter;
      char_no = next_char_counter;
      // Update current_line
      get_current_line(input_program, current_line);

      if (!is_terminate(last_char[0], input_program)) {
        fread(last_char, 1, 1, input_program);
      }

    } else if (state == state_zero) {
      // START Zero state
      if (isspace(last_char[0]) || is_terminate(last_char[0], input_program)) {
        token =
            create_token(token_type_int, accum, line_no, char_no, current_line);
      } else if (last_char[0] == '%') {
        last_char[0] = move_to_newline(input_program);
        next_char_counter = -1;
        next_line_counter++;
        state = state_looking;

        token =
            create_token(token_type_int, accum, line_no, char_no, current_line);
      } else if (isdigit(last_char[0])) {
        sprintf(error_msg, "Integers do not have leading zeros: 0%s",
                last_char);
        raise_LexicalError(next_filename, line_no, char_no, current_line,
                           state_zero, error_msg);
        return NULL;
      } else {
        sprintf(error_msg, "Invalid character after a zero: 0%s", last_char);
        raise_LexicalError(next_filename, line_no, char_no, current_line,
                           state_zero, error_msg);
        return NULL;
      }

      strcpy(accum, "\0");
      state = state_looking;

      if (!is_terminate(last_char[0], input_program)) {
        fread(last_char, 1, 1, input_program);
      }

    } else if (state == state_integer) {
      // START Integer state
      if (isspace(last_char[0]) || is_terminate(last_char[0], input_program)) {
        int temp_integer = 0;
        sscanf(accum, "%d", &temp_integer);
        if (temp_integer > 2147483647) {
          strcpy(error_msg, "Integer must be smaller than 2147483648");
          raise_LexicalError(next_filename, line_no, char_no, current_line,
                             state_integer, error_msg);
          return NULL;
        }

        token =
            create_token(token_type_int, accum, line_no, char_no, current_line);
        strcpy(accum, "\0");
        state = state_looking;
        fseeko(input_program, -1, SEEK_CUR);
      } else if (isdigit(last_char[0])) {
        strcat(accum, last_char);
      } else if (last_char[0] == '%') {
        last_char[0] = move_to_newline(input_program);
        next_char_counter = -1;
        next_line_counter++;
        state = state_looking;

        token =
            create_token(token_type_int, accum, line_no, char_no, current_line);
        strcpy(accum, "\0");
      } else {
        sprintf(error_msg, "Invalid character in integer: %s%s", accum,
                last_char);
        raise_LexicalError(next_filename, line_no, char_no, current_line,
                           state_integer, error_msg);
        return NULL;
      }

      if (!is_terminate(last_char[0], input_program)) {
        fread(last_char, 1, 1, input_program);
      }

    } else if (is_punct_state(state)) {
      // START Punctuation state
      token = create_token(get_punct_token_type(last_char[0]), accum, line_no,
                           char_no, current_line);
      strcpy(accum, "\0");
      state = state_looking;

    } else if (is_operation_state(state)) {
      // START Operator state
      token = create_token(get_operation_token_type(last_char[0]), accum,
                           line_no, char_no, current_line);
      strcpy(accum, "\0");
      state = state_looking;

    } else if (state == state_ids_and_reserved) {
      // START Identifier and Reserved state
      if (isspace(last_char[0]) || is_terminate(last_char[0], input_program) ||
          last_char[0] == '%') {
        if (strlen(accum) > 256) {
          if (strlen(accum) > 256) {
            strcpy(error_msg, "Identifier exceeds 256 character limit.");
            raise_LexicalError(next_filename, line_no, char_no, current_line,
                               state_ids_and_reserved, error_msg);
            return NULL;
          }
        }

        if (strcmp(accum, "if") == 0) {
          token = create_token(token_type_keyword_if, accum, line_no, char_no,
                               current_line);
        } else if (strcmp(accum, "else") == 0) {
          token = create_token(token_type_keyword_else, accum, line_no, char_no,
                               current_line);
        } else if (strcmp(accum, "f") == 0) {
          token = create_token(token_type_keyword_f, accum, line_no, char_no,
                               current_line);
        } else if (strcmp(accum, "returns") == 0) {
          token = create_token(token_type_keyword_returns, accum, line_no,
                               char_no, current_line);
        } else if (strcmp(accum, "integer") == 0) {
          token = create_token(token_type_typename_integer, accum, line_no,
                               char_no, current_line);
        } else if (strcmp(accum, "boolean") == 0) {
          token = create_token(token_type_typename_boolean, accum, line_no,
                               char_no, current_line);
        } else if (strcmp(accum, "print") == 0) {
          token = create_token(token_type_id_print, accum, line_no, char_no,
                               current_line);
        } else if (strcmp(accum, "true") == 0) {
          token = create_token(token_type_bool, accum, line_no, char_no,
                               current_line);
        } else if (strcmp(accum, "false") == 0) {
          token = create_token(token_type_bool, accum, line_no, char_no,
                               current_line);
        } else {
          token = create_token(token_type_id, accum, line_no, char_no,
                               current_line);
        }

        strcpy(accum, "\0");
        state = state_looking;

        if (last_char[0] == '%') {
          last_char[0] = move_to_newline(input_program);
          next_char_counter = -1;
          next_line_counter++;
        } else {
          fseeko(input_program, -1, SEEK_CUR);
        }
      } else if (isalpha(last_char[0])) {
        strcat(accum, last_char);
      } else if (last_char[0] == '$') {
        strcat(accum, last_char);
      } else if (last_char[0] == '_') {
        strcat(accum, last_char);
      } else if (isdigit(last_char[0])) {
        strcat(accum, last_char);
      } else {
        sprintf(error_msg, "Invalid identifier character: %s%s", accum,
                last_char);
        raise_LexicalError(next_filename, line_no, char_no, current_line,
                           state_ids_and_reserved, error_msg);
        return NULL;
      }

      if (!is_terminate(last_char[0], input_program)) {
        fread(last_char, 1, 1, input_program);
      }
    }
  }

  set_token_filename(token, next_filename);
  return token;
}
