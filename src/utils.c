/*
 * Just some common utils that aren't specific to the compiler.
 */
#include <string.h>

char *get_basename(char *path) {
  char *base = strrchr(path, '/');
  return base ? base + 1 : path;
}
