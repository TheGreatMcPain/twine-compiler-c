#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <token_twine.h>
#include <twine_enum.h>

void print_token(struct Token *token) {
  int token_type = token->token_type;

  printf("TokenType: %s, value: %s\n", TwineEnumStrings[token_type],
         token->value);
  printf("LineNo: %d, CharNo: %d\n", token->line_no, token->char_no);
  if (token->current_line) {
    printf("CurrentLine: %s\n", token->current_line);
  }
  printf("\n");
}

struct Token *create_token(int token_type, char *value, int line_no,
                           int char_no, char *current_line) {
  struct Token *token_ptr = malloc(sizeof(struct Token));
  token_ptr->token_type = token_type;
  token_ptr->value = malloc(sizeof(char) * (strlen(value) + 1));
  token_ptr->line_no = line_no;
  token_ptr->char_no = char_no;
  token_ptr->current_line = malloc(sizeof(char) * (strlen(current_line) + 1));
  strcpy(token_ptr->value, value);
  strcpy(token_ptr->current_line, current_line);
  token_ptr->filename = NULL;
  return token_ptr;
}

void set_token_filename(struct Token *token, char *filename) {
  size_t filename_size = strlen(filename);
  token->filename = (char *)malloc(sizeof(char) * (filename_size + 1));

  strcpy(token->filename, filename);
}

// Create a new token copy of 'input_token'.
struct Token *copy_token(struct Token *input_token) {
  struct Token *result = create_token(
      input_token->token_type, input_token->value, input_token->line_no,
      input_token->char_no, input_token->current_line);
  result->filename = strdup(input_token->filename);

  return result;
}

// Properly free memory allocated by create_token()
void free_token(struct Token *token_pointer) {
  if (token_pointer == NULL) {
    return;
  }

  // First free value
  if (token_pointer->value != NULL) {
    free(token_pointer->value);
    token_pointer->value = NULL;
  }

  // free current_line
  if (token_pointer->current_line) {
    free(token_pointer->current_line);
    token_pointer->current_line = NULL;
  }

  if (token_pointer->filename) {
    free(token_pointer->filename);
    token_pointer->filename = NULL;
  }

  // Then free the token pointer
  free(token_pointer);
  token_pointer = NULL;
}
