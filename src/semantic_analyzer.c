#include "symbol_table.h"
#include "twine_errors.h"
#include <ast_nodes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <twine_enum.h>

// Error and Warning functions
static char error_msg_buf[1024] = {'\0'};

static char **error_messages = NULL;
static int num_of_errors = 0;
static char **warning_messages = NULL;
static int num_of_warnings = 0;

void add_to_error_message(struct Token *token, char **error_msg,
                          const char *error_msg_to_append) {
  char message[4096] = {'\0'};

  if (token) {
    create_error_message(token->filename, token->line_no, token->char_no,
                         token->current_line, message);
  }
  strcat(message, error_msg_to_append);

  // Plus one for the null character.
  size_t message_size = strlen(message) + 1;

  // If this is the first error message just allocate it
  // and copy the message.
  if (!(*error_msg)) {
    *error_msg = (char *)malloc(sizeof(char) * message_size);

    strcpy(*error_msg, message);

    return;
  }

  // If this is a new message add it the rear of the message.

  // Plus one for null character.
  size_t error_msg_size = strlen(*error_msg) + 1;

  // Extend (*error_msg) to accomodate the new message.
  // "Plus 1 for newline"
  *error_msg = (char *)realloc(
      *error_msg, sizeof(char) * (error_msg_size + message_size + 1));

  char error_msg_tmp[error_msg_size + message_size + 2];
  error_msg_tmp[0] = '\0';

  strcpy(error_msg_tmp, message);
  strcat(error_msg_tmp, "\n");
  strcat(error_msg_tmp, *error_msg);

  strcpy(*error_msg, error_msg_tmp);
}

void add_error_message(struct Token *token, const char *error_msg) {
  char message[4096] = {'\0'};

  if (token) {
    create_error_message(token->filename, token->line_no, token->char_no,
                         token->current_line, message);
  }
  strcat(message, error_msg);

  if (!error_messages) {
    error_messages = (char **)malloc(sizeof(char *));
    num_of_errors = 0;
  }
  num_of_errors++;
  error_messages =
      (char **)realloc(error_messages, sizeof(char *) * num_of_errors);

  error_messages[num_of_errors - 1] =
      (char *)malloc(sizeof(char) * (strlen(message) + 1));
  strcpy(error_messages[num_of_errors - 1], message);
}

// Takes error messages from string array and returns it
// as one long string.
char *concat_error_messages() {
  char *result = (char *)malloc(sizeof(char));
  result[0] = '\0';

  if (num_of_errors <= 0) {
    return NULL;
  }

  if (!error_messages) {
    return NULL;
  }

  for (int i = 0; i < num_of_errors; i++) {
    size_t string_size = strlen(error_messages[i]) + 1;

    result = (char *)realloc(result,
                             sizeof(char) * (strlen(result) + string_size + 1));
    strcat(result, error_messages[i]);
    strcat(result, "\n");
  }

  return result;
}

void free_error_messages() {
  for (int i = 0; i < num_of_errors; i++) {
    free(error_messages[i]);
  }
  free(error_messages);
}

void add_warning_message(const char *warn_msg) {
  char message[4096] = {'\0'};

  strcat(message, warn_msg);

  if (!warning_messages) {
    warning_messages = (char **)malloc(sizeof(char *));
    num_of_warnings = 0;
  }
  num_of_warnings++;
  warning_messages =
      (char **)realloc(warning_messages, sizeof(char *) * num_of_warnings);

  warning_messages[num_of_warnings - 1] =
      (char *)malloc(sizeof(char) * (strlen(message) + 1));
  strcpy(warning_messages[num_of_warnings - 1], message);
}

void print_warnings() {
  for (int i = 0; i < num_of_warnings; i++) {
    printf("%s\n", warning_messages[i]);
  }
}

void free_warnings() {
  for (int i = 0; i < num_of_warnings; i++) {
    free(warning_messages[i]);
  }
  free(warning_messages);
}

// Semantic Analyzer functions

void analyze_id_node(struct SymbolTable *sym_table, id_node *ast_node,
                     char **cur_error_msg, char *current_func) {
  struct SymbolTableEntry *this_entry =
      symbol_table_get_entry(sym_table, current_func);

  if (symbol_table_entry_is_param(this_entry, ast_node->value)) {
    // Since we know the parameter get's used remove it
    // from the unused parameter list.
    if (symbol_table_entry_is_unused_param(this_entry, ast_node->value)) {
      symbol_table_entry_del_unused_param(this_entry, ast_node->value);
    }

    ast_node->super.semantic_type =
        symbol_table_entry_get_param(this_entry, ast_node->value)
            ->super.semantic_type;
    return;
  }

  sprintf(error_msg_buf, "Unknown identifier: %s", ast_node->value);
  add_to_error_message(NULL, cur_error_msg, error_msg_buf);
  ast_node->super.semantic_type = semantic_type_error;
}

void analyze_integer_node(int_node *ast_node) {
  ast_node->super.semantic_type = semantic_type_integer;
}

void analyze_boolean_node(bool_node *ast_node) {
  ast_node->super.semantic_type = semantic_type_boolean;
}

void analyze_type_node(type_node *ast_node) {
  if (strcmp(ast_node->value, "integer") == 0) {
    ast_node->super.semantic_type = semantic_type_integer;
  } else {
    ast_node->super.semantic_type = semantic_type_boolean;
  }
}

void analyze_name_node(name_node *ast_node) {
  ast_node->super.semantic_type = semantic_type_none;
}

void analyze_param_node(param_node *ast_node) {
  analyze_type_node(ast_node->param_type);
  ast_node->super.semantic_type = ast_node->param_type->super.semantic_type;
}

void analyze_param_list_node(param_list_node *ast_node) {
  ast_node->super.semantic_type = semantic_type_none;

  for (int i = 0; i < ast_node->params_size; i++) {
    analyze_param_node(ast_node->params[i]);
  }
}

void analyze_func_return_node(func_return_node *ast_node) {
  analyze_type_node(ast_node->return_type);
  ast_node->super.semantic_type = ast_node->return_type->super.semantic_type;
}

void analyze_expression(struct SymbolTable *sym_table, ast_node *ast_node,
                        char **cur_error_msg, char *current_func);

void check_binary_exp(struct SymbolTable *sym_table, binary_exp *ast_node,
                      char **cur_error_msg, char *current_func) {
  analyze_expression(sym_table, ast_node->left, cur_error_msg, current_func);
  analyze_expression(sym_table, ast_node->right, cur_error_msg, current_func);

  // Might try to cut down some of the repetative code later.
  switch (ast_node->super.node_type) {
  case ast_node_equals:
    if (ast_node->left->semantic_type == ast_node->right->semantic_type) {
      ast_node->super.semantic_type = semantic_type_boolean;
    } else {
      ast_node->super.semantic_type = semantic_type_error;
    }
    break;
  case ast_node_less_than:
    if (ast_node->left->semantic_type == semantic_type_integer &&
        ast_node->right->semantic_type == semantic_type_integer) {
      ast_node->super.semantic_type = semantic_type_boolean;
    } else {
      ast_node->super.semantic_type = semantic_type_error;
    }
    break;
  case ast_node_or:
    if (ast_node->left->semantic_type == semantic_type_boolean &&
        ast_node->right->semantic_type == semantic_type_boolean) {
      ast_node->super.semantic_type = semantic_type_boolean;
    } else {
      ast_node->super.semantic_type = semantic_type_error;
    }
    break;
  case ast_node_and:
    if (ast_node->left->semantic_type == semantic_type_boolean &&
        ast_node->right->semantic_type == semantic_type_boolean) {
      ast_node->super.semantic_type = semantic_type_boolean;
    } else {
      ast_node->super.semantic_type = semantic_type_error;
    }
    break;
  case ast_node_add:
    if (ast_node->left->semantic_type == semantic_type_integer &&
        ast_node->right->semantic_type == semantic_type_integer) {
      ast_node->super.semantic_type = semantic_type_integer;
    } else {
      ast_node->super.semantic_type = semantic_type_error;
    }
    break;
  case ast_node_subtract:
    if (ast_node->left->semantic_type == semantic_type_integer &&
        ast_node->right->semantic_type == semantic_type_integer) {
      ast_node->super.semantic_type = semantic_type_integer;
    } else {
      ast_node->super.semantic_type = semantic_type_error;
    }
    break;
  case ast_node_multiply:
    if (ast_node->left->semantic_type == semantic_type_integer &&
        ast_node->right->semantic_type == semantic_type_integer) {
      ast_node->super.semantic_type = semantic_type_integer;
    } else {
      ast_node->super.semantic_type = semantic_type_error;
    }
    break;
  case ast_node_divide:
    if (ast_node->left->semantic_type == semantic_type_integer &&
        ast_node->right->semantic_type == semantic_type_integer) {
      ast_node->super.semantic_type = semantic_type_integer;
    } else {
      ast_node->super.semantic_type = semantic_type_error;
    }
    break;
  }

  if (ast_node->super.semantic_type == semantic_type_error) {
    sprintf(error_msg_buf, "Can't compute '%s' with types: '%s' and '%s",
            (TwineEnumStrings[ast_node->super.node_type] + strlen("ast_node_")),
            TwineEnumStrings[ast_node->left->semantic_type],
            TwineEnumStrings[ast_node->right->semantic_type]);
    add_to_error_message(ast_node->super.token, cur_error_msg, error_msg_buf);
  }
}

void check_unary_exp(struct SymbolTable *sym_table, unary_exp *ast_node,
                     char **cur_error_msg, char *current_func) {
  analyze_expression(sym_table, ast_node->next, cur_error_msg, current_func);

  switch (ast_node->super.node_type) {
  case ast_node_not:
    if (ast_node->next->semantic_type == semantic_type_boolean) {
      ast_node->super.semantic_type = semantic_type_boolean;
    } else {
      ast_node->super.semantic_type = semantic_type_error;
    }
    break;
  case ast_node_negative:
    if (ast_node->next->semantic_type == semantic_type_integer) {
      ast_node->super.semantic_type = semantic_type_integer;
    } else {
      ast_node->super.semantic_type = semantic_type_error;
    }
    break;
  }

  if (ast_node->super.semantic_type == semantic_type_error) {
    sprintf(error_msg_buf, "Can't use '%s' operator on type: %s",
            (TwineEnumStrings[ast_node->super.node_type] + strlen("ast_node_")),
            TwineEnumStrings[ast_node->next->semantic_type]);
    add_to_error_message(ast_node->super.token, cur_error_msg, error_msg_buf);
  }
}

void analyze_arg_list_node(struct SymbolTable *sym_table,
                           arg_list_node *ast_node, char **cur_error_msg,
                           char *current_func) {
  for (int i = 0; i < ast_node->args_size; i++) {
    analyze_expression(sym_table, ast_node->args[i], cur_error_msg,
                       current_func);
  }
}

void analyze_func_call_node(struct SymbolTable *sym_table,
                            func_call_node *ast_node, char **cur_error_msg,
                            char *current_func) {
  struct SymbolTableEntry *cur_entry =
      symbol_table_get_entry(sym_table, current_func);
  if (!symbol_table_entry_is_func_call(cur_entry, current_func)) {
    symbol_table_entry_add_func_call(cur_entry, current_func);
  }

  // Function doesn't exist
  if (!symbol_table_is_entry(sym_table, ast_node->name->value)) {
    sprintf(error_msg_buf, "Function '%s' doesn't exist",
            ast_node->name->value);
    add_to_error_message(ast_node->super.token, cur_error_msg, error_msg_buf);

    ast_node->super.semantic_type = semantic_type_error;
    return;
  }

  struct SymbolTableEntry *this_entry =
      symbol_table_get_entry(sym_table, ast_node->name->value);
  ast_node->super.semantic_type = this_entry->semantic_type;

  analyze_arg_list_node(sym_table, ast_node->arg_list, cur_error_msg,
                        current_func);

  if (!symbol_table_entry_is_calling_func(this_entry, current_func)) {
    symbol_table_entry_add_calling_func(this_entry, current_func);
  }

  // There's a chance the argument list will only end up as an identifier.
  // (possibly related to print function calls?)
  if (ast_node->arg_list->args_size != this_entry->param_list->params_size) {
    // Number of args in function call not equal to number of parameters.
    sprintf(error_msg_buf,
            "Number of args in function call: '%s' doesn't match definition",
            ast_node->name->value);
    add_to_error_message(ast_node->super.token, cur_error_msg, error_msg_buf);
    ast_node->super.semantic_type = semantic_type_error;
    return;
  }

  // Check if argument types align with the function definition
  for (int i = 0; i < ast_node->arg_list->args_size; i++) {
    int param_type = this_entry->param_list->params[i]->super.semantic_type;
    int arg_type = ast_node->arg_list->args[i]->semantic_type;

    if (param_type != arg_type) {
      sprintf(error_msg_buf,
              "Incorrect argument type for parameter '%s' in function call "
              "'%s' Expected: %s, Got: %s",
              this_entry->param_list->params[i]->name->value,
              ast_node->name->value, TwineEnumStrings[arg_type],
              TwineEnumStrings[param_type]);
      add_to_error_message(ast_node->super.token, cur_error_msg, error_msg_buf);
      ast_node->super.semantic_type = semantic_type_error;
      return;
    }
  }
}

void analyze_if_node(struct SymbolTable *sym_table, if_node *ast_node,
                     char **cur_error_msg, char *current_func) {
  analyze_expression(sym_table, ast_node->if_exp, cur_error_msg, current_func);
  analyze_expression(sym_table, ast_node->then_exp, cur_error_msg,
                     current_func);
  analyze_expression(sym_table, ast_node->else_exp, cur_error_msg,
                     current_func);

  if (ast_node->if_exp->semantic_type == semantic_type_boolean) {
    if (ast_node->then_exp->semantic_type == semantic_type_error) {
      ast_node->super.semantic_type = semantic_type_error;
    }

    if (ast_node->else_exp->semantic_type == semantic_type_error) {
      ast_node->super.semantic_type = semantic_type_error;
    }

    if (ast_node->then_exp->semantic_type ==
        ast_node->else_exp->semantic_type) {
      ast_node->super.semantic_type = ast_node->then_exp->semantic_type;
    }
  }

  if (ast_node->if_exp->semantic_type != semantic_type_boolean) {
    sprintf(error_msg_buf, "If test must be of type %s",
            TwineEnumStrings[semantic_type_boolean]);
    add_to_error_message(ast_node->super.token, cur_error_msg, error_msg_buf);
    ast_node->super.semantic_type = semantic_type_error;
  }

  if (ast_node->then_exp->semantic_type != ast_node->else_exp->semantic_type) {
    sprintf(error_msg_buf, "Then and else types must match. Then: %s, Else: %s",
            TwineEnumStrings[ast_node->then_exp->semantic_type],
            TwineEnumStrings[ast_node->else_exp->semantic_type]);
    add_to_error_message(NULL, cur_error_msg, error_msg_buf);
    ast_node->super.semantic_type = semantic_type_error;
  }
}

void analyze_body_node(struct SymbolTable *sym_table, body_node *ast_node,
                       char **cur_error_msg, char *current_func) {
  for (int i = 0; i < ast_node->expressions_size; i++) {
    analyze_expression(sym_table, ast_node->expressions[i], cur_error_msg,
                       current_func);
  }

  ast_node->super.semantic_type =
      ast_node->expressions[ast_node->expressions_size - 1]->semantic_type;
}

void analyze_expression(struct SymbolTable *sym_table, ast_node *ast_node,
                        char **cur_error_msg, char *current_func) {
  switch (ast_node->node_type) {
  case ast_node_equals:
    check_binary_exp(sym_table, (binary_exp *)ast_node, cur_error_msg,
                     current_func);
    break;
  case ast_node_less_than:
    check_binary_exp(sym_table, (binary_exp *)ast_node, cur_error_msg,
                     current_func);
    break;
  case ast_node_or:
    check_binary_exp(sym_table, (binary_exp *)ast_node, cur_error_msg,
                     current_func);
    break;
  case ast_node_and:
    check_binary_exp(sym_table, (binary_exp *)ast_node, cur_error_msg,
                     current_func);
    break;
  case ast_node_add:
    check_binary_exp(sym_table, (binary_exp *)ast_node, cur_error_msg,
                     current_func);
    break;
  case ast_node_subtract:
    check_binary_exp(sym_table, (binary_exp *)ast_node, cur_error_msg,
                     current_func);
    break;
  case ast_node_multiply:
    check_binary_exp(sym_table, (binary_exp *)ast_node, cur_error_msg,
                     current_func);
    break;
  case ast_node_divide:
    check_binary_exp(sym_table, (binary_exp *)ast_node, cur_error_msg,
                     current_func);
    break;
  case ast_node_not:
    check_unary_exp(sym_table, (unary_exp *)ast_node, cur_error_msg,
                    current_func);
    break;
  case ast_node_negative:
    check_unary_exp(sym_table, (unary_exp *)ast_node, cur_error_msg,
                    current_func);
    break;
  case ast_node_func_call:
    analyze_func_call_node(sym_table, (func_call_node *)ast_node, cur_error_msg,
                           current_func);
    break;
  case ast_node_if:
    analyze_if_node(sym_table, (if_node *)ast_node, cur_error_msg,
                    current_func);
    break;
  case ast_node_id:
    analyze_id_node(sym_table, (id_node *)ast_node, cur_error_msg,
                    current_func);
    break;
  case ast_node_bool:
    analyze_boolean_node((bool_node *)ast_node);
    break;
  case ast_node_int:
    analyze_integer_node((int_node *)ast_node);
    break;
  }
}

void analyze_def_node(struct SymbolTable *sym_table, def_node *ast_node,
                      char **cur_error_msg) {
  param_list_node *param_list = ast_node->param_list;
  struct Token *token = ast_node->super.token;

  // Check for duplicate parameter names.
  for (int x = 0; x < param_list->params_size; x++) {
    for (int y = 0; y < param_list->params_size; y++) {
      param_node *param_x = param_list->params[x];
      param_node *param_y = param_list->params[y];

      if (y != x && (strcmp(param_x->name->value, param_y->name->value) == 0)) {
        sprintf(error_msg_buf,
                "Duplicate parameter name: '%s' in function: '%s'",
                param_x->name->value, ast_node->name->value);
        add_to_error_message(NULL, cur_error_msg, error_msg_buf);

        // Update symbol table entry type.
        ast_node->super.semantic_type = semantic_type_error;
        symbol_table_get_entry(sym_table, ast_node->name->value)
            ->semantic_type = ast_node->super.semantic_type;
      }
    }
  }

  // Analyze body_node
  analyze_body_node(sym_table, ast_node->body, cur_error_msg,
                    ast_node->name->value);

  // Compare semantic types for body and return values.
  // set error if they don't match.
  if (ast_node->body->super.semantic_type !=
      ast_node->func_return->super.semantic_type) {
    if (ast_node->body->super.semantic_type == semantic_type_error) {
      sprintf(error_msg_buf, "\nError in '%s' in file ", ast_node->name->value);
      create_error_message(token->filename, token->line_no, token->char_no,
                           token->current_line, error_msg_buf);
      // Strip the newline since it'll get added later anyway.
      error_msg_buf[strlen(error_msg_buf) - 1] = '\0';
      add_to_error_message(NULL, cur_error_msg, error_msg_buf);
    } else {
      sprintf(error_msg_buf,
              "Function '%s' returns wrong type\n"
              "Expected: %s, Returned: %s",
              ast_node->name->value,
              TwineEnumStrings[ast_node->func_return->super.semantic_type],
              TwineEnumStrings[ast_node->body->super.semantic_type]);
      add_to_error_message(NULL, cur_error_msg, error_msg_buf);
    }

    // Update symbol table entry type.
    ast_node->super.semantic_type = semantic_type_error;
    symbol_table_get_entry(sym_table, ast_node->name->value)->semantic_type =
        ast_node->super.semantic_type;
  }
}

void analyze_def_list_node(struct SymbolTable *sym_table,
                           def_list_node *ast_node) {
  ast_node->super.semantic_type = semantic_type_none;

  // Process def_nodes and initialize symbol_table
  for (int i = 0; i < ast_node->def_array_size; i++) {
    def_node *current_def_node = ast_node->def_array[i];

    analyze_name_node(current_def_node->name);
    analyze_func_return_node(current_def_node->func_return);
    analyze_param_list_node(current_def_node->param_list);

    current_def_node->super.semantic_type = semantic_type_function;

    symbol_table_add_entry(sym_table, current_def_node);
  }

  // Actually analyze def_nodes and check if any of them produce errors.
  for (int i = 0; i < ast_node->def_array_size; i++) {
    def_node *current_def_node = ast_node->def_array[i];
    char *cur_error_msg = NULL;

    struct SymbolTableEntry *entry =
        symbol_table_get_entry(sym_table, current_def_node->name->value);

    if (!entry->is_analyzed) {
      analyze_def_node(sym_table, current_def_node, &cur_error_msg);

      if (current_def_node->super.semantic_type == semantic_type_error) {
        add_error_message(NULL, cur_error_msg);
        ast_node->super.semantic_type = semantic_type_error;
      }
      entry->is_analyzed = true;
    }
    if (cur_error_msg) {
      free(cur_error_msg);
    }
  }

  // Check for duplicate function names
  if (sym_table->dup_func_names_size > 0) {
    for (int i = 0; i < sym_table->dup_func_names_size; i++) {
      sprintf(error_msg_buf, "Duplicate function: '%s'",
              sym_table->dup_func_names[i]);
      add_error_message(NULL, error_msg_buf);
    }

    ast_node->super.semantic_type = semantic_type_error;
  }

  // Handle warnings: unused functions and unused parameters.
  for (int i = 0; i < sym_table->entries_size; i++) {
    struct SymbolTableEntry *entry = sym_table->entries[i];
    bool header = false;

    if (entry->calling_funcs_size == 0 && strcmp(entry->name, "print") != 0 &&
        strcmp(entry->name, "main") != 0) {
      sprintf(error_msg_buf, "Warnings in '%s': Line %d, Column %d",
              entry->name, entry->token->line_no, entry->token->char_no);
      add_warning_message(error_msg_buf);
      header = true;
      sprintf(error_msg_buf, "Function is unused");
      add_warning_message(error_msg_buf);
    }

    if (entry->unused_params_size > 0 && strcmp(entry->name, "print") != 0) {
      if (!header) {
        sprintf(error_msg_buf, "Warnings in '%s': Line %d, Column %d",
                entry->name, entry->token->line_no, entry->token->char_no);
        add_warning_message(error_msg_buf);       
      }
      sprintf(error_msg_buf, "Unused parameters: ");

      for (int i = 0; i < entry->unused_params_size; i++) {
        param_node *unused_param = entry->unused_params[i];
        strcat(error_msg_buf, unused_param->name->value);
        if (i < entry->unused_params_size) {
          strcat(error_msg_buf, ", ");
        }
      }
      add_warning_message(error_msg_buf);
    }
  }
}

struct SymbolTable *analyze_semantics(ast_node *ast) {
  struct SymbolTable *symbol_table = create_symbol_table();

  // Something must of went wrong if 'ast' isn't a def_list_node
  if (ast->node_type != ast_node_def_list) {
    return NULL;
  }

  analyze_def_list_node(symbol_table, (def_list_node *)ast);

  // Print warning messages.
  if (num_of_warnings > 0) {
    print_warnings();
    free_warnings();
  }

  if (ast->semantic_type == semantic_type_error) {
    char *error_msg = concat_error_messages();

    if (!symbol_table_is_entry(symbol_table, "main")) {
      sprintf(error_msg_buf, "Missing 'main' function");
      add_to_error_message(NULL, &error_msg, error_msg_buf);
    }

    raise_SemanticError(error_msg);
    free(error_msg);
    free_error_messages();
    symbol_table_free(symbol_table);
    return NULL;
  } else {
    if (!symbol_table_is_entry(symbol_table, "main")) {
      sprintf(error_msg_buf, "Missing 'main' function");
      raise_SemanticError(error_msg_buf);
      symbol_table_free(symbol_table);
      return NULL;
    }
  }

  return symbol_table;
}
