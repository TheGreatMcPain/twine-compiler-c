#include <first_follow_sets.h>
#include <grammar.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <twine_enum.h>

// USED BY 'get_follow_set()'
// Allows us to prevent 'get_follow_set()' from infinite recursion.
static bool get_follow_set_started[256];

// Grabs the 'first' if a non_term via recursion
void get_first_of_nonterm(int non_term, int new_first_set[256],
                          int *new_first_set_size, int first_sets[256][256],
                          int first_sets_sizes[256], int enum_grammar[256][256],
                          int enum_grammar_size) {
  *new_first_set_size = 0;

  for (int prod_i = 0; prod_i < enum_grammar_size; prod_i++) {
    int current_non_term = enum_grammar[prod_i][0];

    if (current_non_term == non_term) {
      if (is_non_terminal(enum_grammar[prod_i][1])) {
        int temp_new_first_set[256];
        int temp_new_first_set_size = 0;

        get_first_of_nonterm(enum_grammar[prod_i][1], temp_new_first_set,
                             &temp_new_first_set_size, first_sets,
                             first_sets_sizes, enum_grammar, enum_grammar_size);

        for (int temp_i = 0; temp_i < temp_new_first_set_size; temp_i++) {
          new_first_set[*new_first_set_size] = temp_new_first_set[temp_i];
          (*new_first_set_size)++;
        }
      } else {
        new_first_set[*new_first_set_size] = enum_grammar[prod_i][1];
        (*new_first_set_size)++;
      }
    }
  }
}

// Copies entire first_set of a non_term onto a different first_set.
void append_first_set(int prod_i_to, int non_term_from,
                      int first_sets[256][256], int first_sets_sizes[256],
                      int enum_grammar[256][256], int enum_grammar_size) {

  for (int prod_i = 0; prod_i < enum_grammar_size; prod_i++) {
    int current_non_term = enum_grammar[prod_i][0];

    if (current_non_term == non_term_from) {
      for (int i = 1; i < first_sets_sizes[prod_i]; i++) {
        first_sets[prod_i_to][first_sets_sizes[prod_i_to]] =
            first_sets[prod_i][i];
        first_sets_sizes[prod_i_to]++;

        first_sets[prod_i_to][first_sets_sizes[prod_i_to]] = -1;
      }
    }
  }
}

// Get first sets for the grammar
void get_first_sets(int first_sets[256][256], int enum_grammar[256][256],
                    int enum_grammar_size) {
  int first_sets_sizes[256];

  // Initialize first_sets
  for (int i = 0; i < enum_grammar_size; i++) {
    first_sets[i][0] = enum_grammar[i][0];
    first_sets_sizes[i] = 1;
  }

  for (int prod_i = 0; prod_i < enum_grammar_size; prod_i++) {
    // Process terminal
    if (!is_non_terminal(enum_grammar[prod_i][1])) {
      // If the value is a semantic action assume the production is just
      // 'epsilon'
      if (!is_semantic_action(enum_grammar[prod_i][1])) {
        first_sets[prod_i][first_sets_sizes[prod_i]] = enum_grammar[prod_i][1];
      } else {
        first_sets[prod_i][first_sets_sizes[prod_i]] = token_type_epsilon;
      }
      first_sets_sizes[prod_i]++;

      first_sets[prod_i][first_sets_sizes[prod_i]] = -1;
    } else {
      // Get first of non_terminal
      int non_term_first_set[256];
      int non_term_first_set_size = 0;

      get_first_of_nonterm(enum_grammar[prod_i][1], non_term_first_set,
                           &non_term_first_set_size, first_sets,
                           first_sets_sizes, enum_grammar, enum_grammar_size);

      // Copy results if 'get_first_of_nonterm()' onto current first set.
      for (int temp_i = 0; temp_i < non_term_first_set_size; temp_i++) {
        first_sets[prod_i][first_sets_sizes[prod_i]] =
            non_term_first_set[temp_i];
        first_sets_sizes[prod_i]++;

        first_sets[prod_i][first_sets_sizes[prod_i]] = -1;
      }
    }
  }
}

// Copies the first set of 'non_term' onto 'to_first_set'
// Also check if the resulting first set contains an epsilon
bool follow_set_rule2_helper(int non_term, int *to_first_set,
                             int *to_first_set_size, int first_sets[256][256],
                             int num_first_sets) {
  *to_first_set_size = 0;
  bool has_epsilon = false;

  for (int prod_i = 0; prod_i < num_first_sets; prod_i++) {
    int *current_first_set = first_sets[prod_i];

    if (current_first_set[0] == non_term) {
      for (int i = 1; current_first_set[i] != -1; i++) {
        if (current_first_set[i] == token_type_epsilon) {
          has_epsilon = true;
        } else {
          to_first_set[*to_first_set_size] = current_first_set[i];
          (*to_first_set_size)++;

          to_first_set[*to_first_set_size] = -1;
        }
      }
    }
  }

  return has_epsilon;
}

bool is_in_array(int needle, int *array, int array_size) {
  for (int i = 0; i < array_size; i++) {
    if (array[i] == needle) {
      return true;
    }
  }

  return false;
}

// Get the follow_set of a non_terminal
void get_follow_set(int non_term, int follow_sets[256][256],
                    int follow_sets_sizes[256], int first_sets[256][256],
                    int enum_grammar[256][256], int enum_grammar_size) {
  // Follow rules
  //
  // 1. First put "$" in Follow(S) (S is the start symbol)
  // 2. If A -> aBb then everything in FIRST(b) except for "ε" in FOLLOW(B)
  // 3. If A -> aB then everything in FOLLOW(A) is in FOLLOW(B)
  // 4. If A -> aBb where FIRST(b) contains "ε"
  //    then everything in FOLLOW(A) is in FOLLOW(B)
  int production[256];
  int production_size = 0;

  int first_set[256];
  int first_set_size = 0;

  // Rule 1
  if (non_term == nonterm_program) {
    if (!get_follow_set_started[non_term]) {
      get_follow_set_started[non_term] = true;
      follow_sets[non_term][follow_sets_sizes[non_term]] =
          token_type_end_of_file;
      follow_sets_sizes[non_term]++;
      follow_sets[non_term][follow_sets_sizes[non_term]] = -1;
    }
    return;
  }

  get_follow_set_started[non_term] = true;

  for (int prod_i = 0; prod_i < enum_grammar_size; prod_i++) {
    production_size = 0;
    for (int i = 1; enum_grammar[prod_i][i] != -1; i++) {
      if (!(is_semantic_action(enum_grammar[prod_i][i]) ||
            enum_grammar[prod_i][i] == token_type_epsilon)) {
        production[production_size] = enum_grammar[prod_i][i];
        production_size++;
      }
    }

    int cur_non_term = enum_grammar[prod_i][0];

    for (int item_i = 0; item_i < production_size; item_i++) {
      if (production[item_i] != non_term) {
        continue;
      }
      int non_term_index = item_i;

      if (non_term_index + 1 == production_size) {
        // Rule 3
        if (!get_follow_set_started[cur_non_term]) {
          get_follow_set(cur_non_term, follow_sets, follow_sets_sizes,
                         first_sets, enum_grammar, enum_grammar_size);
        }

        for (int i = 0; i < follow_sets_sizes[cur_non_term]; i++) {
          if (!is_in_array(follow_sets[cur_non_term][i], follow_sets[non_term],
                           follow_sets_sizes[non_term])) {
            follow_sets[non_term][follow_sets_sizes[non_term]] =
                follow_sets[cur_non_term][i];
            follow_sets_sizes[non_term]++;
          }
        }
        continue;
      }

      // Rule 2
      if (is_non_terminal(production[non_term_index + 1])) {
        bool has_epsilon = follow_set_rule2_helper(
            production[non_term_index + 1], first_set, &first_set_size,
            first_sets, enum_grammar_size);

        // Rule 4
        if (has_epsilon) {
          get_follow_set(cur_non_term, follow_sets, follow_sets_sizes,
                         first_sets, enum_grammar, enum_grammar_size);

          for (int i = 0; i < follow_sets_sizes[cur_non_term]; i++) {
            first_set[first_set_size] = follow_sets[cur_non_term][i];
            first_set_size++;
          }
        }

        for (int i = 0; i < first_set_size; i++) {
          if (!is_in_array(first_set[i], follow_sets[non_term],
                           follow_sets_sizes[non_term])) {
            follow_sets[non_term][follow_sets_sizes[non_term]] = first_set[i];
            follow_sets_sizes[non_term]++;
          }
        }
      } else {
        if (!is_in_array(production[non_term_index + 1], follow_sets[non_term],
                         follow_sets_sizes[non_term])) {
          follow_sets[non_term][follow_sets_sizes[non_term]] =
              production[non_term_index + 1];
          follow_sets_sizes[non_term]++;
        }
      }
    }
  }
}

// Get follow_sets of a given grammar
void get_follow_sets(int follow_sets[256][256], int first_sets[256][256],
                     int enum_grammar[256][256], int enum_grammar_size) {
  const int NonTermArray[] = {FOREACH_NONTERMINAL(GENERATE_ENUM)};
  const int NonTermArraySize = sizeof(NonTermArray) / sizeof(*NonTermArray);

  int follow_set_sizes[256];
  for (int i = 0; i < 256; i++) {
    follow_set_sizes[i] = 0;
  }

  for (int i = 0; i < 256; i++) {
    for (int o = 0; o < 256; o++) {
      follow_sets[i][o] = 0;
    }
  }

  for (int i = 0; i < 256; i++) {
    get_follow_set_started[i] = false;
  }

  for (int non_term_i = 0; non_term_i < NonTermArraySize; non_term_i++) {
    get_follow_set(NonTermArray[non_term_i], follow_sets, follow_set_sizes,
                   first_sets, enum_grammar, enum_grammar_size);

    follow_sets[NonTermArray[non_term_i]]
               [follow_set_sizes[NonTermArray[non_term_i]]] = -1;
  }
}

void print_first_sets() {
  int enum_grammar[256][256];
  int enum_grammar_size = get_grammar_as_enums(enum_grammar);
  int first_sets[256][256];
  char padding[256] = {"\0"};

  // Get first sets.
  get_first_sets(first_sets, enum_grammar, enum_grammar_size);

  for (int prod_i = 0; prod_i < enum_grammar_size; prod_i++) {
    int *first_set = first_sets[prod_i];
    int current_non_term = first_set[0];

    int NonTermStringSize = strlen(TwineEnumStrings[current_non_term]) + 1;

    printf("%s ->\n", TwineEnumStrings[current_non_term]);

    for (int first_set_i = 1; first_set[first_set_i] != -1; first_set_i++) {
      strcpy(padding, "");
      int counter = NonTermStringSize;
      while (counter > 0) {
        strcat(padding, " ");
        counter--;
      }
      printf("%s%s\n", padding, TwineEnumStrings[first_set[first_set_i]]);
    }
  }
}

void print_follow_sets() {
  int enum_grammar[256][256];
  int enum_grammar_size = get_grammar_as_enums(enum_grammar);
  char padding[256] = {"\0"};
  int follow_sets[256][256];
  int first_sets[256][256];

  const int NonTermArray[] = {FOREACH_NONTERMINAL(GENERATE_ENUM)};
  size_t NonTermArraySize = sizeof(NonTermArray) / sizeof(*NonTermArray);

  get_first_sets(first_sets, enum_grammar, enum_grammar_size);
  get_follow_sets(follow_sets, first_sets, enum_grammar, enum_grammar_size);

  for (size_t i = 0; i < NonTermArraySize; i++) {
    int NonTermStringSize = strlen(TwineEnumStrings[NonTermArray[i]]) + 1;

    printf("%s ->\n", TwineEnumStrings[NonTermArray[i]]);

    for (int follow_set_i = 0; follow_sets[NonTermArray[i]][follow_set_i] != -1;
         follow_set_i++) {
      strcpy(padding, "");
      int counter = NonTermStringSize;
      while (counter > 0) {
        strcat(padding, " ");
        counter--;
      }
      printf("%s%s\n", padding,
             TwineEnumStrings[follow_sets[NonTermArray[i]][follow_set_i]]);
    }
  }
}
