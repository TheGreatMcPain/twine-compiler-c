#include <first_follow_sets.h>
#include <grammar.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <twine_enum.h>

// Frees parse_table (compliments gen_parse_table())
void free_parse_table(int *parse_table[256][256]) {
  for (int x = 0; x < 256; x++) {
    for (int y = 0; y < 256; y++) {
      if (parse_table[x][y]) {
        free(parse_table[x][y]);
        parse_table[x][y] = NULL;
      }
    }
  }
}

// Create parse_table.
// parse_table: A matrix which will contain a production or NULL.
void gen_parse_table(int *parse_table[256][256]) {
  int production[256];
  int production_size = 0;
  int first_sets[256][256];
  int follow_sets[256][256];
  int enum_grammar[256][256];
  int enum_grammar_size = get_grammar_as_enums(enum_grammar);

  get_first_sets(first_sets, enum_grammar, enum_grammar_size);
  get_follow_sets(follow_sets, first_sets, enum_grammar, enum_grammar_size);

  // Init parse_table
  for (int x = 0; x < 256; x++) {
    for (int y = 0; y < 256; y++) {
      parse_table[x][y] = NULL;
    }
  }

  // Get productions and map them onto the table.
  for (int prod_i = 0; prod_i < enum_grammar_size; prod_i++) {
    int cur_non_term = enum_grammar[prod_i][0];

    production_size = 0;
    for (int i = 1; enum_grammar[prod_i][i] != -1; i++) {
      production[production_size] = enum_grammar[prod_i][i];
      production_size++;
    }

    // If production is null use follow set.
    if (production_size == 1 && (production[0] == token_type_epsilon ||
                                 is_semantic_action(production[0]))) {
      for (int i = 0; follow_sets[cur_non_term][i] != -1; i++) {
        int item = follow_sets[cur_non_term][i];

        if (!parse_table[cur_non_term][item]) {
          parse_table[cur_non_term][item] =
              (int *)malloc(sizeof(int) * (production_size + 1));
          memcpy(parse_table[cur_non_term][item], production,
                 sizeof(int) * production_size);

          // Add terminator
          parse_table[cur_non_term][item][production_size] = -1;
        }
      }
    } else {
      // If production is not null use first set.
      for (int i = 1; first_sets[prod_i][i] != -1; i++) {
        int item = first_sets[prod_i][i];

        if (item != token_type_epsilon) {
          parse_table[cur_non_term][item] =
              (int *)malloc(sizeof(int) * (production_size + 1));
          memcpy(parse_table[cur_non_term][item], production,
                 sizeof(int) * production_size);

          // Add terminator
          parse_table[cur_non_term][item][production_size] = -1;
        }
      }
    }
  }
}

void print_parse_table() {
  int *parse_table[256][256];
  char print_string[1024];

  const int non_term_array[] = {FOREACH_NONTERMINAL(GENERATE_ENUM)};
  int non_term_array_size = sizeof(non_term_array) / sizeof(*non_term_array);

  const int term_array[] = {FOREACH_TOKENTYPE(GENERATE_ENUM)};
  int term_array_size = sizeof(term_array) / sizeof(*term_array);

  gen_parse_table(parse_table);

  for (int non_term_i = 0; non_term_i < non_term_array_size; non_term_i++) {
    for (int term_i = 0; term_i < term_array_size; term_i++) {
      int cur_non_term = non_term_array[non_term_i];
      int cur_term = term_array[term_i];

      sprintf(print_string, "%s,%s -> ", TwineEnumStrings[cur_non_term],
              TwineEnumStrings[cur_term]);

      if (!parse_table[cur_non_term][cur_term]) {
        strcat(print_string, "NULL");
        continue;
      } else {
        for (int i = 0; parse_table[cur_non_term][cur_term][i] != -1; i++) {
          strcat(print_string,
                 TwineEnumStrings[parse_table[cur_non_term][cur_term][i]]);
          strcat(print_string, " ");
        }
      }

      printf("%s\n", print_string);
    }
  }

  free_parse_table(parse_table);
}
