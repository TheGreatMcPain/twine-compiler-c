#include <grammar.h>
#include <scanner_utils.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <token_twine.h>
#include <twine_enum.h>

// String array representing the grammar
char *productions[] = {
    "nonterm_program -> nonterm_definition_list semantic_action_make_def_list",

    "nonterm_definition_list -> nonterm_definition nonterm_definition_list_",

    "nonterm_definition_list_ -> nonterm_definition_list",
    "nonterm_definition_list_ -> token_type_epsilon",

    "nonterm_definition -> token_type_id semantic_action_make_name '=' \
                    token_type_keyword_f '(' nonterm_parameter_list \
                            semantic_action_make_param_list \
                            nonterm_function_return ')' \
                    nonterm_body semantic_action_make_body semantic_action_make_def",

    "nonterm_function_return -> token_type_keyword_returns nonterm_type \
                    semantic_action_make_type semantic_action_make_func_return",

    "nonterm_body -> nonterm_print_expression nonterm_body",
    "nonterm_body -> nonterm_expression",

    "nonterm_print_expression -> token_type_id_print semantic_action_make_name \
                    '(' nonterm_expression ')' semantic_action_make_func_call",

    "nonterm_parameter_list -> nonterm_formal_parameters",
    "nonterm_parameter_list -> token_type_epsilon",

    "nonterm_formal_parameters -> nonterm_id_with_type \
                                  nonterm_formal_parameters_",

    "nonterm_formal_parameters_ -> ',' nonterm_formal_parameters",
    "nonterm_formal_parameters_ -> token_type_epsilon",

    "nonterm_id_with_type -> token_type_id semantic_action_make_id \
                    ':' nonterm_type semantic_action_make_type \
                                     semantic_action_make_param",

    "nonterm_type -> token_type_typename_integer",
    "nonterm_type -> token_type_typename_boolean",

    "nonterm_expression -> nonterm_simple_expression nonterm_expression_",

    "nonterm_expression_ -> '=' nonterm_expression \
                                semantic_action_make_equals",
    "nonterm_expression_ -> '<' nonterm_expression \
                                semantic_action_make_less_than",
    "nonterm_expression_ -> token_type_epsilon",

    "nonterm_simple_expression -> nonterm_term nonterm_simple_expression_",

    "nonterm_simple_expression_ -> '|' nonterm_simple_expression \
                                       semantic_action_make_or",
    "nonterm_simple_expression_ -> '+' nonterm_simple_expression \
                                       semantic_action_make_add",
    "nonterm_simple_expression_ -> '-' nonterm_simple_expression \
                                       semantic_action_make_subtract",
    "nonterm_simple_expression_ -> token_type_epsilon",

    "nonterm_term -> nonterm_factor nonterm_term_",

    "nonterm_term_ -> '^' nonterm_term semantic_action_make_and",
    "nonterm_term_ -> '*' nonterm_term semantic_action_make_multiply",
    "nonterm_term_ -> '/' nonterm_term semantic_action_make_divide",
    "nonterm_term_ -> token_type_epsilon",

    "nonterm_factor -> nonterm_literal",
    "nonterm_factor -> '~' nonterm_factor semantic_action_make_not",
    "nonterm_factor -> '-' nonterm_factor semantic_action_make_negative",
    "nonterm_factor -> token_type_id nonterm_argument_list_",
    "nonterm_factor -> token_type_keyword_if '(' nonterm_expression ')' \
                       nonterm_expression token_type_keyword_else nonterm_expression \
                                                 semantic_action_make_if",
    "nonterm_factor -> '(' nonterm_expression ')'",

    "nonterm_argument_list_ -> '(' semantic_action_make_name \
                                   nonterm_argument_list ')' \
                               semantic_action_make_func_call",
    "nonterm_argument_list_ -> semantic_action_make_id",

    "nonterm_argument_list -> semantic_action_make_arg_list",
    "nonterm_argument_list -> nonterm_formal_arguments \
                              semantic_action_make_arg_list",

    "nonterm_formal_arguments -> nonterm_expression nonterm_formal_arguments_",

    "nonterm_formal_arguments_ -> ',' nonterm_formal_arguments",
    "nonterm_formal_arguments_ -> token_type_epsilon",

    "nonterm_literal -> token_type_bool semantic_action_make_bool",
    "nonterm_literal -> token_type_int semantic_action_make_int",
    NULL};

bool is_semantic_action(int input_value) {
  const int SemanticActionArray[] = {FOREACH_SEMANTICACTION(GENERATE_ENUM)};

  size_t SemActArraySize =
      sizeof(SemanticActionArray) / sizeof(*SemanticActionArray);

  for (size_t i = 0; i < SemActArraySize; i++) {
    if (input_value == SemanticActionArray[i]) {
      return true;
    }
  }

  return false;
}

bool is_non_terminal(int input_value) {
  const int NonTermArray[] = {FOREACH_NONTERMINAL(GENERATE_ENUM)};

  size_t NonTermArraySize = sizeof(NonTermArray) / sizeof(*NonTermArray);

  for (size_t i = 0; i < NonTermArraySize; i++) {
    if (input_value == NonTermArray[i]) {
      return true;
    }
  }

  return false;
}

void print_grammar_as_enums() {
  int enums_grammar[256][256];
  char print_string[1024];

  int grammar_size = get_grammar_as_enums(enums_grammar);

  for (int i = 0; i < grammar_size; i++) {
    sprintf(print_string, "%s -> ", TwineEnumStrings[enums_grammar[i][0]]);

    for (int index = 1; enums_grammar[i][index] != -1; index++) {
      strcat(print_string, " ");
      strcat(print_string, TwineEnumStrings[enums_grammar[i][index]]);
    }
    printf("%s\n", print_string);
  }
}

// Converts grammar into an matrix of integers (enum).
int get_grammar_as_enums(int grammar_as_enums[256][256]) {
  char production[1024];
  int prod_i = 0;

  while (productions[prod_i]) {
    int num_of_enums = 1;

    strcpy(production, productions[prod_i]);

    char *t = strtok(production, " ");

    while (t != NULL) {
      if (strncmp(t, "->", 2) != 0) {
        grammar_as_enums[prod_i][num_of_enums - 1] = grammar_item_to_enum(t);
        num_of_enums++;

        // Some sort of end marker.
        grammar_as_enums[prod_i][num_of_enums - 1] = -1;
      }

      t = strtok(NULL, " ");
    }

    prod_i++;
  }

  return prod_i;
}

int grammar_item_to_enum(const char *grammar_item) {
  int results = grammar_item_to_semantic_action(grammar_item);
  if (results != -1) {
    return results;
  }

  results = grammar_item_to_non_term(grammar_item);
  if (results != -1) {
    return results;
  }

  results = grammar_item_to_term_token_type(grammar_item);
  if (results != -1) {
    return results;
  }

  return -1;
}

int grammar_item_to_term_token_type(const char *grammar_item) {
  const int TokenTypeArray[] = {FOREACH_TOKENTYPE(GENERATE_ENUM)};

  size_t TokenTypeArraySize = sizeof(TokenTypeArray) / sizeof(*TokenTypeArray);

  for (size_t i = 0; i < TokenTypeArraySize; i++) {
    if (strcmp(grammar_item, TwineEnumStrings[TokenTypeArray[i]]) == 0) {
      return TokenTypeArray[i];
    }
  }

  // Prevent possible segfault when we try to access anything out of bounds.
  if (strlen(grammar_item) < 3) {
    return -1;
  }

  // Make use of functions in scanner_utils
  if (!(grammar_item[0] == '\'' && grammar_item[2] == '\'')) {
    return -1;
  }

  if (is_punct(grammar_item[1])) {
    return get_punct_token_type(grammar_item[1]);
  }

  if (is_operation(grammar_item[1])) {
    return get_operation_token_type(grammar_item[1]);
  }

  return -1;
}

int grammar_item_to_non_term(const char *grammar_item) {
  const int NonTermArray[] = {FOREACH_NONTERMINAL(GENERATE_ENUM)};

  size_t NonTermArraySize = sizeof(NonTermArray) / sizeof(*NonTermArray);

  for (size_t i = 0; i < NonTermArraySize; i++) {
    if (strcmp(grammar_item, TwineEnumStrings[NonTermArray[i]]) == 0) {
      return NonTermArray[i];
    }
  }

  return -1;
}

// Convert string from grammar representation into it's enum
int grammar_item_to_semantic_action(const char *grammar_item) {
  const int SemanticActionArray[] = {FOREACH_SEMANTICACTION(GENERATE_ENUM)};

  size_t SemActArraySize =
      sizeof(SemanticActionArray) / sizeof(*SemanticActionArray);

  for (size_t i = 0; i < SemActArraySize; i++) {
    if (strcmp(grammar_item, TwineEnumStrings[SemanticActionArray[i]]) == 0) {
      return SemanticActionArray[i];
    }
  }

  return -1;
}
