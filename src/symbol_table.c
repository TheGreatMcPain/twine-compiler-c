#include "twine_enum.h"
#include <ast_nodes.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <symbol_table.h>
#include <token_twine.h>

struct SymbolTableEntry *symbol_table_create_entry(def_node *ast_node) {
  struct SymbolTableEntry *new_entry =
      (struct SymbolTableEntry *)malloc(sizeof(struct SymbolTableEntry));

  new_entry->name = ast_node->name->value;
  new_entry->token = ast_node->super.token;
  new_entry->semantic_type = ast_node->func_return->super.semantic_type;
  new_entry->param_list = ast_node->param_list;
  new_entry->is_duplicate = false;
  new_entry->is_analyzed = false;

  // Allocate calling_funcs and func_calls
  new_entry->calling_funcs = (char **)malloc(sizeof(char *));
  new_entry->calling_funcs_size = 0;
  new_entry->func_calls = (char **)malloc(sizeof(char *));
  new_entry->func_calls_size = 0;

  // Allocate unused_params (start with all and will calculate them later.)
  new_entry->unused_params = (param_node **)malloc(
      sizeof(param_node *) * ast_node->param_list->params_size);
  new_entry->unused_params_size = ast_node->param_list->params_size;

  for (int i = 0; i < new_entry->unused_params_size; i++) {
    new_entry->unused_params[i] = ast_node->param_list->params[i];
  }

  return new_entry;
}

struct SymbolTable *create_symbol_table() {
  struct SymbolTable *new_table =
      (struct SymbolTable *)malloc(sizeof(struct SymbolTable));

  new_table->entries =
      (struct SymbolTableEntry **)malloc(sizeof(struct SymbolTableEntry *));
  new_table->entries_size = 0;

  new_table->dup_func_names = (char **)malloc(sizeof(char *));
  new_table->dup_func_names_size = 0;

  return new_table;
}

struct SymbolTableEntry *symbol_table_get_entry(struct SymbolTable *sym_table,
                                                char *func_name) {
  for (int i = 0; i < sym_table->entries_size; i++) {
    if (strcmp(sym_table->entries[i]->name, func_name) == 0) {
      return sym_table->entries[i];
    }
  }

  return NULL;
}

bool symbol_table_entry_is_calling_func(
    struct SymbolTableEntry *sym_table_entry, char *func_name) {
  for (int i = 0; i < sym_table_entry->calling_funcs_size; i++) {
    if (strcmp(sym_table_entry->calling_funcs[i], func_name) == 0) {
      return true;
    }
  }

  return false;
}

void symbol_table_entry_add_calling_func(
    struct SymbolTableEntry *sym_table_entry, char *func_name) {
  size_t func_name_size = strlen(func_name) + 1;

  sym_table_entry->calling_funcs_size++;
  sym_table_entry->calling_funcs =
      (char **)realloc(sym_table_entry->calling_funcs,
                       sizeof(char *) * sym_table_entry->calling_funcs_size);

  sym_table_entry->calling_funcs[sym_table_entry->calling_funcs_size - 1] =
      (char *)malloc(sizeof(char) * func_name_size);

  strcpy(
      sym_table_entry->calling_funcs[sym_table_entry->calling_funcs_size - 1],
      func_name);
}

bool symbol_table_entry_is_func_call(struct SymbolTableEntry *sym_table_entry,
                                     char *func_name) {
  for (int i = 0; i < sym_table_entry->func_calls_size; i++) {
    if (strcmp(sym_table_entry->func_calls[i], func_name) == 0) {
      return true;
    }
  }

  return false;
}

void symbol_table_entry_add_func_call(struct SymbolTableEntry *sym_table_entry,
                                      char *func_name) {
  size_t func_name_size = strlen(func_name) + 1;

  sym_table_entry->func_calls_size++;
  sym_table_entry->func_calls =
      (char **)realloc(sym_table_entry->func_calls,
                       sizeof(char *) * sym_table_entry->func_calls_size);

  sym_table_entry->func_calls[sym_table_entry->func_calls_size - 1] =
      (char *)malloc(sizeof(char) * func_name_size);

  strcpy(sym_table_entry->func_calls[sym_table_entry->func_calls_size - 1],
         func_name);
}

param_node *
symbol_table_entry_get_param(struct SymbolTableEntry *sym_table_entry,
                             char *param_name) {
  for (int i = 0; i < sym_table_entry->param_list->params_size; i++) {
    if (strcmp(sym_table_entry->param_list->params[i]->name->value,
               param_name) == 0) {
      return sym_table_entry->param_list->params[i];
    }
  }

  return NULL;
}

bool symbol_table_entry_is_param(struct SymbolTableEntry *sym_table_entry,
                                 char *param_name) {
  if (symbol_table_entry_get_param(sym_table_entry, param_name)) {
    return true;
  }
  return false;
}

bool symbol_table_entry_is_unused_param(
    struct SymbolTableEntry *sym_table_entry, char *param_name) {
  for (int i = 0; i < sym_table_entry->param_list->params_size; i++) {
    if (strcmp(sym_table_entry->param_list->params[i]->name->value,
               param_name) == 0) {
      return true;
    }
  }
  return false;
}

void symbol_table_entry_del_unused_param(
    struct SymbolTableEntry *sym_table_entry, char *param_name) {
  int new_unused_param_size = 0;

  for (int i = 0; i < sym_table_entry->unused_params_size; i++) {
    if (strcmp(sym_table_entry->unused_params[i]->name->value, param_name) !=
        0) {
      sym_table_entry->unused_params[new_unused_param_size] =
          sym_table_entry->unused_params[i];
      new_unused_param_size++;
    }
  }

  // Realloc unused_params
  sym_table_entry->unused_params =
      (param_node **)realloc(sym_table_entry->unused_params,
                             sizeof(param_node *) * new_unused_param_size);
  sym_table_entry->unused_params_size = new_unused_param_size;
}

void symbol_table_entry_print(struct SymbolTableEntry *sym_table_entry) {
  char message[4096] = {'\0'};

  strcpy(message, sym_table_entry->name);
  strcat(message, "\n");

  if (sym_table_entry->is_duplicate) {
    strcat(message, "  Duplicate Function Names!\n");
  }

  strcat(message, "  Type: ");
  strcat(message, TwineEnumStrings[sym_table_entry->semantic_type]);
  strcat(message, "\n  Parameters: ");

  for (int i = 0; i < sym_table_entry->param_list->params_size; i++) {
    param_node *param = sym_table_entry->param_list->params[i];

    strcat(message, "\n    name: ");
    strcat(message, param->name->value);
    strcat(message, "; type: ");
    strcat(message, param->param_type->value);
  }

  strcat(message, "\n  Functions Called By This: ");
  for (int i = 0; i < sym_table_entry->func_calls_size; i++) {
    strcat(message, "\n    name: ");
    strcat(message, sym_table_entry->func_calls[i]);
  }
  strcat(message, "\n  Functions That Call This: ");
  for (int i = 0; i < sym_table_entry->calling_funcs_size; i++) {
    strcat(message, "\n    name: ");
    strcat(message, sym_table_entry->calling_funcs[i]);
  }

  printf("%s\n", message);
}

void symbol_table_entry_free(struct SymbolTableEntry *sym_table_entry) {
  // These pointer point to a def_node's memory locations,
  // so we shouldn't need to free them here.
  sym_table_entry->name = NULL;
  sym_table_entry->token = NULL;
  sym_table_entry->param_list = NULL;

  // Same for the unused params array
  for (int i = 0; i < sym_table_entry->unused_params_size; i++) {
    sym_table_entry->unused_params[i] = NULL;
  }
  free(sym_table_entry->unused_params);

  // calling_funcs and func_calls are actually malloc'd
  for (int i = 0; i < sym_table_entry->calling_funcs_size; i++) {
    free(sym_table_entry->calling_funcs[i]);
  }
  free(sym_table_entry->calling_funcs);

  for (int i = 0; i < sym_table_entry->func_calls_size; i++) {
    free(sym_table_entry->func_calls[i]);
  }
  free(sym_table_entry->func_calls);

  free(sym_table_entry);
}

bool symbol_table_is_entry(struct SymbolTable *sym_table, char *func_name) {
  struct SymbolTableEntry *result =
      symbol_table_get_entry(sym_table, func_name);

  if (result) {
    return true;
  }

  return false;
}

void symbol_table_add_entry(struct SymbolTable *sym_table, def_node *ast_node) {
  struct SymbolTableEntry *entry =
      symbol_table_get_entry(sym_table, ast_node->name->value);

  // If get_entry doesn't return NULL we know this one is duplicate.
  if (entry) {
    // Mark this entry as "has duplicate", and return.
    if (entry->is_duplicate) {
      return;
    }

    entry->is_duplicate = true;
    sym_table->dup_func_names_size++;
    sym_table->dup_func_names =
        (char **)realloc(sym_table->dup_func_names,
                         sizeof(char *) * sym_table->dup_func_names_size);

    sym_table->dup_func_names[sym_table->dup_func_names_size - 1] = entry->name;

    return;
  }

  entry = symbol_table_create_entry(ast_node);

  sym_table->entries_size++;
  sym_table->entries = (struct SymbolTableEntry **)realloc(sym_table->entries,
      sizeof(struct SymbolTableEntry *) * sym_table->entries_size);
  sym_table->entries[sym_table->entries_size - 1] = entry;
}

void symbol_table_print(struct SymbolTable *sym_table) {
  for (int i = 0; i < sym_table->entries_size; i++) {
    symbol_table_entry_print(sym_table->entries[i]);
  }
}

void symbol_table_free(struct SymbolTable *sym_table) {
  for (int i = 0; i < sym_table->entries_size; i++) {
    symbol_table_entry_free(sym_table->entries[i]);
  }
  free(sym_table->entries);

  for (int i = 0; i < sym_table->dup_func_names_size; i++) {
    sym_table->dup_func_names[i] = NULL;
  }
  free(sym_table->dup_func_names);

  free(sym_table);
}
