#include <ast_nodes.h>
#include <semantic_actions.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <token_twine.h>
#include <twine_enum.h>

// AST NODE STACK

void ast_node_stack_init(struct ast_node_stack *input_stack) {
  input_stack->array = (ast_node **)malloc(sizeof(ast_node *));
  input_stack->array_size = 0;
}

void ast_node_stack_free(struct ast_node_stack *input_stack) {
  if (input_stack->array) {
    free(input_stack->array);
    input_stack->array = NULL;
  }

  free(input_stack);
  input_stack = NULL;
}

ast_node *ast_node_stack_top(struct ast_node_stack *stack) {
  if (stack->array_size <= 0) {
    return NULL;
  }
  return stack->array[stack->array_size - 1];
}

ast_node *ast_node_stack_pop(struct ast_node_stack *stack) {
  if (stack->array_size <= 0) {
    return NULL;
  }
  ast_node *result = stack->array[stack->array_size - 1];
  stack->array_size--;
  stack->array = (ast_node **)realloc(stack->array,
                                      sizeof(ast_node *) * stack->array_size);

  return result;
}

void ast_node_stack_push(struct ast_node_stack *stack, ast_node *value) {
  // Set value's semantic_type to -1
  value->semantic_type = -1;

  stack->array_size++;
  stack->array = (ast_node **)realloc(stack->array,
                                      sizeof(ast_node *) * stack->array_size);
  stack->array[stack->array_size - 1] = value;
}

// INT STACK

void int_stack_init(struct int_stack *input_stack) {
  input_stack->array = (int *)malloc(sizeof(int));
  input_stack->array_size = 0;
}

void int_stack_free(struct int_stack *input_stack) {
  if (input_stack->array) {
    free(input_stack->array);
    input_stack->array = NULL;
  }

  free(input_stack);
  input_stack = NULL;
}

int int_stack_top(struct int_stack *stack) {
  if (stack->array_size <= 0) {
    return -1;
  }
  return stack->array[stack->array_size - 1];
}

int int_stack_pop(struct int_stack *stack) {
  if (stack->array_size <= 0) {
    return -1;
  }
  int result = stack->array[stack->array_size - 1];
  stack->array_size--;
  stack->array = (int *)realloc(stack->array, sizeof(int) * stack->array_size);

  return result;
}

void int_stack_push(struct int_stack *stack, int value) {
  stack->array_size++;
  stack->array = (int *)realloc(stack->array, sizeof(int) * stack->array_size);
  stack->array[stack->array_size - 1] = value;
}

void reverse_ast_array(ast_node **ast_array, int ast_array_size) {
  ast_node *temp_ast_array[ast_array_size];
  int temp_ast_array_size = 0;

  // Copy ast_array to temp array in reverse.
  for (int i = ast_array_size - 1; i >= 0; i--) {
    temp_ast_array[temp_ast_array_size] = ast_array[i];
    temp_ast_array_size++;
  }

  // Copy temp array back to ast_array.
  for (int i = 0; i < temp_ast_array_size; i++) {
    ast_array[i] = temp_ast_array[i];
  }
}

// SEMANTIC ACTIONS

void make_def_list(struct ast_node_stack *semantic_stack) {
  // Initialize def_list AST_Node
  def_list_node *new_def_list_node =
      (def_list_node *)malloc(sizeof(def_list_node));
  new_def_list_node->super.node_type = ast_node_def_list;
  new_def_list_node->def_array = (def_node **)malloc(sizeof(def_node *));
  new_def_list_node->def_array_size = 0;

  // Grab all definition AST_Nodes from the semantic_stack, and
  // append them to def_list's node_array.
  while (ast_node_stack_top(semantic_stack)->node_type == ast_node_def) {
    def_node *popped_ast_node = (def_node *)ast_node_stack_pop(semantic_stack);
    if (!popped_ast_node) {
      break;
    }

    new_def_list_node->def_array_size++;
    new_def_list_node->def_array = (def_node **)realloc(
        new_def_list_node->def_array,
        sizeof(def_node *) * new_def_list_node->def_array_size);
    new_def_list_node->def_array[new_def_list_node->def_array_size - 1] =
        popped_ast_node;

    if (!ast_node_stack_top(semantic_stack)) {
      break;
    }
  }

  // Assign def_list's token value
  new_def_list_node->super.token = copy_token(
      new_def_list_node->def_array[new_def_list_node->def_array_size - 1]
          ->super.token);

  // Reverse def_list's node_array so that the definitions are in order.
  reverse_ast_array((ast_node **)new_def_list_node->def_array,
                     new_def_list_node->def_array_size);

  // Push def_list onto semantic_stack
  ast_node_stack_push(semantic_stack, (ast_node *)new_def_list_node);
}

void make_def(struct ast_node_stack *semantic_stack) {
  // Grab definition_node parts from semantic_stack
  body_node *body = (body_node *)ast_node_stack_pop(semantic_stack);
  func_return_node *func_return =
      (func_return_node *)ast_node_stack_pop(semantic_stack);
  param_list_node *param_list =
      (param_list_node *)ast_node_stack_pop(semantic_stack);
  name_node *id = (name_node *)ast_node_stack_pop(semantic_stack);

  def_node *new_def_node = (def_node *)malloc(sizeof(def_node));
  new_def_node->super.node_type = ast_node_def;
  new_def_node->name = id;
  new_def_node->super.token = copy_token(id->super.token);
  new_def_node->param_list = param_list;
  new_def_node->func_return = func_return;
  new_def_node->body = body;

  ast_node_stack_push(semantic_stack, (ast_node *)new_def_node);
}

void make_id(struct Token *matched_terminal,
             struct ast_node_stack *semantic_stack) {
  id_node *new_id_node = (id_node *)malloc(sizeof(id_node));
  size_t value_size = strlen(matched_terminal->value);

  new_id_node->super.node_type = ast_node_id;
  new_id_node->super.token = copy_token(matched_terminal);
  new_id_node->value = (char *)malloc(sizeof(char) * (value_size + 1));
  strcpy(new_id_node->value, matched_terminal->value);

  ast_node_stack_push(semantic_stack, (ast_node *)new_id_node);
}

void make_param_list(struct ast_node_stack *semantic_stack) {
  // Initialize param_list_node
  param_list_node *new_node =
      (param_list_node *)malloc(sizeof(param_list_node));
  new_node->super.node_type = ast_node_param_list;

  new_node->params = (param_node **)malloc(sizeof(param_node *));
  new_node->params_size = 0;

  // Append new param_nodes to new_node's params array.
  while (ast_node_stack_top(semantic_stack)->node_type == ast_node_param) {
    param_node *popped_node = (param_node *)ast_node_stack_pop(semantic_stack);
    if (!popped_node) {
      break;
    }

    new_node->params_size++;
    new_node->params = (param_node **)realloc(
        new_node->params, sizeof(param_node *) * new_node->params_size);
    new_node->params[new_node->params_size - 1] = popped_node;
  }

  struct Token *token = NULL;
  if (new_node->params_size > 0) {
    token =
        copy_token(new_node->params[new_node->params_size - 1]->super.token);
  }
  new_node->super.token = token;

  reverse_ast_array((ast_node **)new_node->params, new_node->params_size);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_func_return(struct ast_node_stack *semantic_stack) {
  func_return_node *new_node =
      (func_return_node *)malloc(sizeof(func_return_node));

  new_node->super.node_type = ast_node_func_return;
  new_node->return_type = (type_node *)ast_node_stack_pop(semantic_stack);
  new_node->super.token = copy_token(new_node->return_type->super.token);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

// Make body helper.
bool is_expression(ast_node *node) {
  int node_type = node->node_type;

  const int expression_nodes[] = {
      ast_node_func_call, ast_node_equals,   ast_node_less_than,
      ast_node_or,        ast_node_add,      ast_node_subtract,
      ast_node_and,       ast_node_multiply, ast_node_divide,
      ast_node_bool,      ast_node_int,      ast_node_not,
      ast_node_negative,  ast_node_if,       ast_node_id};
  int expression_nodes_size =
      sizeof(expression_nodes) / sizeof(*expression_nodes);

  for (int i = 0; i < expression_nodes_size; i++) {
    if (expression_nodes[i] == node_type) {
      return true;
    }
  }

  return false;
}

void make_body(struct ast_node_stack *semantic_stack) {
  // Initialize body_node
  body_node *new_node = (body_node *)malloc(sizeof(body_node));

  new_node->super.node_type = ast_node_body;

  new_node->expressions = (ast_node **)malloc(sizeof(ast_node *));
  new_node->expressions_size = 0;

  while (is_expression(ast_node_stack_top(semantic_stack))) {
    ast_node *popped_node = ast_node_stack_pop(semantic_stack);
    if (!popped_node) {
      break;
    }

    new_node->expressions_size++;
    new_node->expressions = (ast_node **)realloc(
        new_node->expressions, sizeof(ast_node *) * new_node->expressions_size);
    new_node->expressions[new_node->expressions_size - 1] = popped_node;
  }

  new_node->super.token =
      copy_token(new_node->expressions[new_node->expressions_size - 1]->token);

  reverse_ast_array(new_node->expressions, new_node->expressions_size);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_param(struct ast_node_stack *semantic_stack) {
  param_node *new_node = (param_node *)malloc(sizeof(param_node));

  new_node->super.node_type = ast_node_param;
  new_node->param_type = (type_node *)ast_node_stack_pop(semantic_stack);
  new_node->name = (name_node *)ast_node_stack_pop(semantic_stack);
  new_node->super.token = copy_token(new_node->name->super.token);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_type(struct Token *matched_terminal,
               struct ast_node_stack *semantic_stack) {
  type_node *new_node = (type_node *)malloc(sizeof(type_node));
  size_t value_size = strlen(matched_terminal->value);

  new_node->super.node_type = ast_node_type;
  new_node->super.token = copy_token(matched_terminal);
  new_node->value = (char *)malloc(sizeof(char) * (value_size + 1));
  strcpy(new_node->value, matched_terminal->value);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_equals(struct ast_node_stack *semantic_stack) {
  equals_node *new_node = (equals_node *)malloc(sizeof(equals_node));

  new_node->super.right = ast_node_stack_pop(semantic_stack);
  new_node->super.left = ast_node_stack_pop(semantic_stack);

  new_node->super.super.node_type = ast_node_equals;
  new_node->super.super.token = copy_token(new_node->super.left->token);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_less_then(struct ast_node_stack *semantic_stack) {
  less_than_node *new_node = (less_than_node *)malloc(sizeof(equals_node));

  new_node->super.right = ast_node_stack_pop(semantic_stack);
  new_node->super.left = ast_node_stack_pop(semantic_stack);

  new_node->super.super.node_type = ast_node_less_than;
  new_node->super.super.token = copy_token(new_node->super.left->token);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_or(struct ast_node_stack *semantic_stack) {
  or_node *new_node = (or_node *)malloc(sizeof(equals_node));

  new_node->super.right = ast_node_stack_pop(semantic_stack);
  new_node->super.left = ast_node_stack_pop(semantic_stack);

  new_node->super.super.node_type = ast_node_or;
  new_node->super.super.token = copy_token(new_node->super.left->token);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_and(struct ast_node_stack *semantic_stack) {
  and_node *new_node = (and_node *)malloc(sizeof(equals_node));

  new_node->super.right = ast_node_stack_pop(semantic_stack);
  new_node->super.left = ast_node_stack_pop(semantic_stack);

  new_node->super.super.node_type = ast_node_and;
  new_node->super.super.token = copy_token(new_node->super.left->token);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

// TODO Implement order-of-operations fixer.

void make_add(struct ast_node_stack *semantic_stack) {
  add_node *new_node = (add_node *)malloc(sizeof(equals_node));

  new_node->super.right = ast_node_stack_pop(semantic_stack);
  new_node->super.left = ast_node_stack_pop(semantic_stack);

  new_node->super.super.node_type = ast_node_add;
  new_node->super.super.token = copy_token(new_node->super.left->token);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_subtract(struct ast_node_stack *semantic_stack) {
  subtract_node *new_node = (subtract_node *)malloc(sizeof(equals_node));

  new_node->super.right = ast_node_stack_pop(semantic_stack);
  new_node->super.left = ast_node_stack_pop(semantic_stack);

  new_node->super.super.node_type = ast_node_subtract;
  new_node->super.super.token = copy_token(new_node->super.left->token);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_multiply(struct ast_node_stack *semantic_stack) {
  multiply_node *new_node = (multiply_node *)malloc(sizeof(equals_node));

  new_node->super.right = ast_node_stack_pop(semantic_stack);
  new_node->super.left = ast_node_stack_pop(semantic_stack);

  new_node->super.super.node_type = ast_node_multiply;
  new_node->super.super.token = copy_token(new_node->super.left->token);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_divide(struct ast_node_stack *semantic_stack) {
  divide_node *new_node = (divide_node *)malloc(sizeof(equals_node));

  new_node->super.right = ast_node_stack_pop(semantic_stack);
  new_node->super.left = ast_node_stack_pop(semantic_stack);

  new_node->super.super.node_type = ast_node_divide;
  new_node->super.super.token = copy_token(new_node->super.left->token);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_not(struct ast_node_stack *semantic_stack) {
  not_node *new_node = (not_node *)malloc(sizeof(equals_node));

  new_node->super.next = ast_node_stack_pop(semantic_stack);

  new_node->super.super.node_type = ast_node_not;
  new_node->super.super.token = copy_token(new_node->super.next->token);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_negative(struct ast_node_stack *semantic_stack) {
  negative_node *new_node = (negative_node *)malloc(sizeof(equals_node));

  new_node->super.next = ast_node_stack_pop(semantic_stack);

  new_node->super.super.node_type = ast_node_negative;
  new_node->super.super.token = copy_token(new_node->super.next->token);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_int(struct Token *matched_terminal,
              struct ast_node_stack *semantic_stack) {
  int_node *new_node = (int_node *)malloc(sizeof(int_node));
  size_t value_size = strlen(matched_terminal->value);

  new_node->super.node_type = ast_node_int;
  new_node->super.token = copy_token(matched_terminal);
  new_node->value = (char *)malloc(sizeof(char) * (value_size + 1));
  strcpy(new_node->value, matched_terminal->value);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_bool(struct Token *matched_terminal,
               struct ast_node_stack *semantic_stack) {
  bool_node *new_node = (bool_node *)malloc(sizeof(int_node));
  size_t value_size = strlen(matched_terminal->value);

  new_node->super.node_type = ast_node_bool;
  new_node->super.token = copy_token(matched_terminal);
  new_node->value = (char *)malloc(sizeof(char) * (value_size + 1));
  strcpy(new_node->value, matched_terminal->value);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_name(struct Token *matched_terminal,
               struct ast_node_stack *semantic_stack) {
  name_node *new_node = (name_node *)malloc(sizeof(int_node));
  size_t value_size = strlen(matched_terminal->value);

  new_node->super.node_type = ast_node_name;
  new_node->super.token = copy_token(matched_terminal);
  new_node->value = (char *)malloc(sizeof(char) * (value_size + 1));
  strcpy(new_node->value, matched_terminal->value);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_arg_list(struct ast_node_stack *semantic_stack) {
  arg_list_node *new_node = (arg_list_node *)malloc(sizeof(arg_list_node));

  new_node->super.node_type = ast_node_arg_list;

  new_node->args = (ast_node **)malloc(sizeof(ast_node *));
  new_node->args_size = 0;

  while (is_expression(ast_node_stack_top(semantic_stack))) {
    ast_node *popped = ast_node_stack_pop(semantic_stack);
    if (!popped) {
      break;
    }

    new_node->args_size++;
    new_node->args = (ast_node **)realloc(
        new_node->args, sizeof(ast_node *) * new_node->args_size);
    new_node->args[new_node->args_size - 1] = popped;
  }

  new_node->super.token =
      copy_token(new_node->args[new_node->args_size - 1]->token);

  reverse_ast_array(new_node->args, new_node->args_size);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

void make_func_call(struct ast_node_stack *semantic_stack) {
  func_call_node *new_node = (func_call_node *)malloc(sizeof(func_call_node));

  new_node->super.node_type = ast_node_func_call;

  arg_list_node *arg_list = (arg_list_node *)ast_node_stack_pop(semantic_stack);
  name_node *id = (name_node *)ast_node_stack_pop(semantic_stack);

  // Based on the language grammar print function calls don't
  // contain arg_list_nodes, so we will need to create are own arg_list_node.
  if (arg_list->super.node_type != ast_node_arg_list) {
    arg_list_node *new_arg_list = (arg_list_node *)malloc(sizeof(arg_list_node));
    new_arg_list->super.node_type = ast_node_arg_list;
    new_arg_list->args = (ast_node **)malloc(sizeof(ast_node *));
    new_arg_list->args_size = 1;

    new_arg_list->args[0] = (ast_node *)arg_list;
    new_arg_list->super.token = copy_token(arg_list->super.token);

    arg_list = new_arg_list;
  }

  new_node->name = id;
  new_node->arg_list = arg_list;

  new_node->super.token = copy_token(id->super.token);

  ast_node_stack_push(semantic_stack, (ast_node *)new_node);
}

// Calls semantic actions that only push to the semantic_stack.
void matched_term_action(int semantic_action, struct Token *matched_terminal,
                         struct ast_node_stack *semantic_stack) {

  switch (semantic_action) {
  case semantic_action_make_id:
    make_id(matched_terminal, semantic_stack);
    break;
  case semantic_action_make_type:
    make_type(matched_terminal, semantic_stack);
    break;
  case semantic_action_make_bool:
    make_bool(matched_terminal, semantic_stack);
    break;
  case semantic_action_make_int:
    make_int(matched_terminal, semantic_stack);
    break;
  case semantic_action_make_name:
    make_name(matched_terminal, semantic_stack);
    break;
  }
}

// Calls make functions based on the semantic_action value.
void action(int semantic_action, struct ast_node_stack *semantic_stack) {
  switch (semantic_action) {
  case semantic_action_make_def_list:
    make_def_list(semantic_stack);
    break;
  case semantic_action_make_def:
    make_def(semantic_stack);
    break;
  case semantic_action_make_param_list:
    make_param_list(semantic_stack);
    break;
  case semantic_action_make_func_return:
    make_func_return(semantic_stack);
    break;
  case semantic_action_make_body:
    make_body(semantic_stack);
    break;
  case semantic_action_make_param:
    make_param(semantic_stack);
    break;
  case semantic_action_make_equals:
    make_equals(semantic_stack);
    break;
  case semantic_action_make_less_than:
    make_less_then(semantic_stack);
    break;
  case semantic_action_make_or:
    make_or(semantic_stack);
    break;
  case semantic_action_make_and:
    make_and(semantic_stack);
    break;
  case semantic_action_make_add:
    make_add(semantic_stack);
    break;
  case semantic_action_make_subtract:
    make_subtract(semantic_stack);
    break;
  case semantic_action_make_multiply:
    make_multiply(semantic_stack);
    break;
  case semantic_action_make_divide:
    make_divide(semantic_stack);
    break;
  case semantic_action_make_not:
    make_not(semantic_stack);
    break;
  case semantic_action_make_negative:
    make_negative(semantic_stack);
    break;
  case semantic_action_make_func_call:
    make_func_call(semantic_stack);
    break;
  case semantic_action_make_arg_list:
    make_arg_list(semantic_stack);
    break;
  }
}
