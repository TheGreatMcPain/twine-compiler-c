#include "grammar.h"
#include <scanner.h>
#include <semantic_actions.h>
#include <stdio.h>
#include <stdlib.h>
#include <table_generator.h>
#include <token_twine.h>
#include <twine_enum.h>
#include <twine_errors.h>

ast_node *parse(FILE *input_program, char *filename) {
  scanner_set_filename(filename);

  int *parse_table[256][256];

  gen_parse_table(parse_table);

  struct int_stack *parser_stack =
      (struct int_stack *)malloc(sizeof(struct int_stack));
  struct ast_node_stack *semantic_stack =
      (struct ast_node_stack *)malloc(sizeof(struct ast_node_stack));
  struct Token *matched_terminal = NULL;

  char error_msg[4096] = {'\0'};

  int_stack_init(parser_stack);
  ast_node_stack_init(semantic_stack);

  int_stack_push(parser_stack, token_type_end_of_file);
  int_stack_push(parser_stack, nonterm_program);

  int token_value = int_stack_top(parser_stack);

  while (token_value != token_type_end_of_file) {
    token_value = int_stack_top(parser_stack);

    if (token_value == token_type_epsilon) {
      // Pop epsilon and move on
      int_stack_pop(parser_stack);
    } else if (!(is_non_terminal(token_value) ||
                 is_semantic_action(token_value))) {
      struct Token *peeked_token = peek(input_program);
      if (token_value == peeked_token->token_type) {
        int_stack_pop(parser_stack);

        struct Token *token = next(input_program);

        // if (token->token_type == token_type_punct_leftparen) {
        //
        // }
        // if (token->token_type == token_type_punct_rightparen) {
        //
        // }

        if (token->token_type == token_type_id ||
            token->token_type == token_type_bool ||
            token->token_type == token_type_int ||
            token->token_type == token_type_typename_integer ||
            token->token_type == token_type_typename_boolean ||
            token->token_type == token_type_id_print) {

          // Free previous token.
          if (matched_terminal) {
            free_token(matched_terminal);
            matched_terminal = NULL;
          }

          matched_terminal = copy_token(token);
        }
        free_token(token);
      } else {
        sprintf(error_msg, "Expected %s, found %s: %s",
                TwineEnumStrings[token_value],
                TwineEnumStrings[peeked_token->token_type],
                peeked_token->value);
        raise_SyntacticError(peeked_token, error_msg);
        return NULL;
      }
      free_token(peeked_token);
    } else if (is_semantic_action(token_value)) {
      if (token_value == semantic_action_make_id ||
          token_value == semantic_action_make_type ||
          token_value == semantic_action_make_bool ||
          token_value == semantic_action_make_int ||
          token_value == semantic_action_make_name) {
        matched_term_action(token_value, matched_terminal, semantic_stack);
      } else {
        // TODO Help order-of-operations
        action(token_value, semantic_stack);
      }
      int_stack_pop(parser_stack);
    } else {
      struct Token *peeked_token = peek(input_program);
      if (parse_table[token_value][peeked_token->token_type]) {
        int_stack_pop(parser_stack);

        int *symbol_list = parse_table[token_value][peeked_token->token_type];
        int symbol_list_size = 0;

        // Get symbol_list size
        for (int i = 0; symbol_list[i] != -1; i++) {
          symbol_list_size++;
        }

        // Add symbol_list to parser_stack.
        while (symbol_list_size > 0) {
          symbol_list_size--;
          int_stack_push(parser_stack, symbol_list[symbol_list_size]);
        }
      } else {
        sprintf(error_msg, "No transition out of non-terminal %s for %s",
                TwineEnumStrings[token_value],
                TwineEnumStrings[peeked_token->token_type]);
        raise_SyntacticError(peeked_token, error_msg);
        return NULL;
      }
      free_token(peeked_token);
    }
  }

  struct Token *peeked_token = peek(input_program);
  if (!(peeked_token->token_type == token_type_end_of_file)) {
    sprintf(error_msg, "Unexpected token at end: %s",
            TwineEnumStrings[peeked_token->token_type]);
    raise_EofError(error_msg);
    return NULL;
  }
  free_token(peeked_token);

  if (semantic_stack->array_size != 1) {
    sprintf(error_msg, "Unexected number of AST nodes: %d",
            semantic_stack->array_size);
    raise_EofError(error_msg);
    return NULL;
  }

  ast_node *complete_ast = ast_node_stack_pop(semantic_stack);

  // Prevent memory leaks
  free_token(matched_terminal);
  int_stack_free(parser_stack);
  ast_node_stack_free(semantic_stack);
  free_parse_table(parse_table);

  return complete_ast;
}
