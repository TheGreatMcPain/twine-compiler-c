/*
 * This contains functions for creating error messages
 * used in this compiler.
 *
 * When an error occurs the message will display these things...
 *
 * 1. The filename
 * 2. Line number
 * 3. Position in the line
 * 4. The contents of the line
 * 5. Where it failed
 * 6. Why it failed
 */
#include <scanner.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <token_twine.h>
#include <twine_enum.h>

void create_error_message(const char *filename, int line_no, int char_no,
                          const char *line_contents, char message[]) {
  char filename_message[1024] = {'\0'};

  // Example: 'filename.twn'2:7
  sprintf(filename_message, "'%s'%d:%d\n", filename, line_no, char_no);
  strcat(message, filename_message);

  // Display line contents
  strcat(message, "    |\n    |");
  strcat(message, line_contents);
  strcat(message, "\n    |");

  int counter = char_no;
  while (counter > 0) {
    strcat(message, " ");
    counter--;
  }

  strcat(message, "^\n");
}

// Scanner error = LexicalError
void raise_LexicalError(const char *filename, int line_no, int char_no,
                        const char *line_contents, int scanner_state,
                        const char *error_msg) {
  char message[4096] = {'\0'}; // 4K should be enough.

  strcat(message, "TwineLexicalError: While scanner in state [");
  strcat(message, TwineEnumStrings[scanner_state]);
  strcat(message, "] ");
  create_error_message(filename, line_no, char_no, line_contents, message);
  strcat(message, "    ");
  strcat(message, error_msg);

  fprintf(stderr, "%s\n", message);
}

// Parser error = SyntaticError
void raise_SyntacticError(struct Token *token, const char *error_msg) {
  char message[4096] = {'\0'};

  strcat(message, "TwineSyntacticError: ");
  create_error_message(token->filename, token->line_no, token->char_no,
                       token->current_line, message);
  strcat(message, "    ");
  strcat(message, error_msg);

  fprintf(stderr, "%s\n", message);
}

// Special parser error = TwineEofError
void raise_EofError(const char *error_msg) {
  char message[4096] = {'\0'};

  strcpy(message, "TwineEofError: ");
  strcat(message, error_msg);

  fprintf(stderr, "%s\n", message);
}

// Semantic Analyzer error = SemanticError
void raise_SemanticError(const char *error_msg) {
  char message[strlen("TwineSemanticError: ") + strlen(error_msg) + 1];

  strcpy(message, "TwineSemanticError: ");
  strcat(message, error_msg);

  fprintf(stderr, "\n%s\n", message);
}
