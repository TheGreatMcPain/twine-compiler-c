#include <scanner.h>
#include <scanner_utils.h>
#include <twine_enum.h>

// Check if a character is a punctuation
bool is_punct(char input) {
  const char puncts[4] = "():,";

  for (int i = 0; i < 4; i++) {
    if (input == puncts[i]) {
      return true;
    }
  }
  return false;
}

// Get token type for punctuation
int get_punct_token_type(char input) {
  int token_type = -1;

  switch (input) {
  case '(':
    token_type = token_type_punct_leftparen;
    break;
  case ')':
    token_type = token_type_punct_rightparen;
    break;
  case ':':
    token_type = token_type_punct_colon;
    break;
  case ',':
    token_type = token_type_punct_comma;
    break;
  default:
    token_type = -1;
  }

  return token_type;
}

// Get token type for punctuation
int get_punct_state(char input) {
  int state = -1;

  switch (input) {
  case '(':
    state = state_leftparen;
    break;
  case ')':
    state = state_rightparen;
    break;
  case ':':
    state = state_colon;
    break;
  case ',':
    state = state_comma;
    break;
  default:
    state = -1;
  }

  return state;
}

bool is_punct_state(int state) {
  const int punct_states[4] = {state_leftparen, state_rightparen, state_colon,
                               state_comma};
  for (int i = 0; i < 4; i++) {
    if (state == punct_states[i]) {
      return true;
    }
  }
  return false;
}

// Check if a character is an operation
bool is_operation(char input) {
  const char operations[9] = "~^|</*-+=";

  for (int i = 0; i < 9; i++) {
    if (input == operations[i]) {
      return true;
    }
  }
  return false;
}

int get_operation_token_type(char input) {
  int token_type = -1;

  switch (input) {
  case '~':
    token_type = token_type_op_not;
    break;
  case '^':
    token_type = token_type_op_and;
    break;
  case '|':
    token_type = token_type_op_or;
    break;
  case '<':
    token_type = token_type_op_lessthan;
    break;
  case '/':
    token_type = token_type_op_divide;
    break;
  case '*':
    token_type = token_type_op_multiply;
    break;
  case '-':
    token_type = token_type_op_minus;
    break;
  case '+':
    token_type = token_type_op_plus;
    break;
  case '=':
    token_type = token_type_op_equals;
    break;
  default:
    token_type = -1;
  }

  return token_type;
}

int get_operation_state(char input) {
  int state = -1;

  switch (input) {
  case '~':
    state = state_op_not;
    break;
  case '^':
    state = state_op_and;
    break;
  case '|':
    state = state_op_or;
    break;
  case '<':
    state = state_op_lessthan;
    break;
  case '/':
    state = state_op_divide;
    break;
  case '*':
    state = state_op_multiply;
    break;
  case '-':
    state = state_op_minus;
    break;
  case '+':
    state = state_op_plus;
    break;
  case '=':
    state = state_op_equals;
    break;
  default:
    state = -1;
  }

  return state;
}

bool is_operation_state(int state) {
  const int op_states[9] = {
      state_op_not,      state_op_and,    state_op_or,
      state_op_lessthan, state_op_divide, state_op_multiply,
      state_op_minus,    state_op_plus,   state_op_equals};
  for (int i = 0; i < 9; i++) {
    if (state == op_states[i]) {
      return true;
    }
  }
  return false;
}

// Helper function which checks
// is_operation, is_punct, or if the input file is EOF.
bool is_terminate(char input_char, FILE *input_file_stream) {
  if (is_operation(input_char) || is_punct(input_char)) {
    return true;
  }

  if (feof(input_file_stream) != 0) {
    return true;
  }

  return false;
}

// Walk filepointer until newline character is reached.
char move_to_newline(FILE *file_stream) {
  char buffer = 0;

  // Prepare buffer.
  fread(&buffer, 1, 1, file_stream);

  // Loop until newline, or EOF.
  while (buffer != '\n' && (feof(file_stream) == 0)) {
    fread(&buffer, 1, 1, file_stream);
  }

  // Return buffer character.
  return buffer;
}
