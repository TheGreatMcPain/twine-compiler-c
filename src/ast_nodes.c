#include "semantic_actions.h"
#include "token_twine.h"
#include "twine_enum.h"
#include <ast_nodes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INDENT_SIZE 2

// PRINT_AST

void print_name_node(name_node *ast_node, int space_size) {
  printf("%*s%s = %s\n", space_size, "",
         TwineEnumStrings[ast_node->super.node_type], ast_node->value);
}

void print_id_node(id_node *ast_node, int space_size) {
  printf("%*s%s = %s\n", space_size, "",
         TwineEnumStrings[ast_node->super.node_type], ast_node->value);
}

void print_type_node(type_node *ast_node, int space_size) {
  printf("%*s%s = %s\n", space_size, "",
         TwineEnumStrings[ast_node->super.node_type], ast_node->value);
}

void print_param_node(param_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.node_type]);

  print_name_node(ast_node->name, space_size + INDENT_SIZE);
  print_type_node(ast_node->param_type, space_size + INDENT_SIZE);
}

void print_param_list_node(param_list_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.node_type]);

  for (int i = 0; i < ast_node->params_size; i++) {
    print_param_node(ast_node->params[i], space_size + INDENT_SIZE);
  }
}

void print_func_return_node(func_return_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.node_type]);

  print_type_node(ast_node->return_type, space_size + INDENT_SIZE);
}

void print_body_node(body_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.node_type]);

  for (int i = 0; i < ast_node->expressions_size; i++) {
    print_ast(ast_node->expressions[i], space_size + INDENT_SIZE);
  }
}

void print_def_node(def_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.node_type]);

  print_name_node(ast_node->name, space_size + INDENT_SIZE);
  print_param_list_node(ast_node->param_list, space_size + INDENT_SIZE);
  print_func_return_node(ast_node->func_return, space_size + INDENT_SIZE);
  print_body_node(ast_node->body, space_size + INDENT_SIZE);
}

void print_def_list_node(def_list_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.node_type]);

  for (int i = 0; i < ast_node->def_array_size; i++) {
    print_def_node(ast_node->def_array[i], space_size + INDENT_SIZE);
  }
}

void print_equals_node(equals_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.super.node_type]);

  print_ast(ast_node->super.left, space_size + INDENT_SIZE);
  print_ast(ast_node->super.right, space_size + INDENT_SIZE);
}

void print_less_than_node(less_than_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.super.node_type]);

  print_ast(ast_node->super.left, space_size + INDENT_SIZE);
  print_ast(ast_node->super.right, space_size + INDENT_SIZE);
}

void print_and_node(and_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.super.node_type]);

  print_ast(ast_node->super.left, space_size + INDENT_SIZE);
  print_ast(ast_node->super.right, space_size + INDENT_SIZE);
}

void print_or_node(or_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.super.node_type]);

  print_ast(ast_node->super.left, space_size + INDENT_SIZE);
  print_ast(ast_node->super.right, space_size + INDENT_SIZE);
}

void print_add_node(add_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.super.node_type]);

  print_ast(ast_node->super.left, space_size + INDENT_SIZE);
  print_ast(ast_node->super.right, space_size + INDENT_SIZE);
}

void print_subtract_node(subtract_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.super.node_type]);

  print_ast(ast_node->super.left, space_size + INDENT_SIZE);
  print_ast(ast_node->super.right, space_size + INDENT_SIZE);
}

void print_multiply_node(multiply_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.super.node_type]);

  print_ast(ast_node->super.left, space_size + INDENT_SIZE);
  print_ast(ast_node->super.right, space_size + INDENT_SIZE);
}

void print_divide_node(divide_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.super.node_type]);

  print_ast(ast_node->super.left, space_size + INDENT_SIZE);
  print_ast(ast_node->super.right, space_size + INDENT_SIZE);
}

void print_not_node(not_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.super.node_type]);

  print_ast(ast_node->super.next, space_size + INDENT_SIZE);
}

void print_negative_node(negative_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.super.node_type]);

  print_ast(ast_node->super.next, space_size + INDENT_SIZE);
}

void print_arg_list_node(arg_list_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.node_type]);

  for (int i = 0; i < ast_node->args_size; i++) {
    print_ast(ast_node->args[i], space_size + INDENT_SIZE);
  }
}

void print_func_call_node(func_call_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.node_type]);

  print_name_node(ast_node->name, space_size + INDENT_SIZE);
  print_arg_list_node(ast_node->arg_list, space_size + INDENT_SIZE);
}

void print_if_node(if_node *ast_node, int space_size) {
  printf("%*s%s\n", space_size, "",
         TwineEnumStrings[ast_node->super.node_type]);

  print_ast(ast_node->if_exp, space_size + INDENT_SIZE);
  print_ast(ast_node->then_exp, space_size + INDENT_SIZE);
  print_ast(ast_node->else_exp, space_size + INDENT_SIZE);
}

void print_int_node(int_node *ast_node, int space_size) {
  printf("%*s%s = %s\n", space_size, "",
         TwineEnumStrings[ast_node->super.node_type], ast_node->value);
}

void print_bool_node(bool_node *ast_node, int space_size) {
  printf("%*s%s = %s\n", space_size, "",
         TwineEnumStrings[ast_node->super.node_type], ast_node->value);
}

void print_ast(ast_node *current_ast_node, int space_size) {
  int ast_type = current_ast_node->node_type;

  switch (ast_type) {
  case ast_node_name:
    print_name_node((name_node *)current_ast_node, space_size);
    break;
  case ast_node_id:
    print_id_node((id_node *)current_ast_node, space_size);
    break;
  case ast_node_param:
    print_param_node((param_node *)current_ast_node, space_size);
    break;
  case ast_node_param_list:
    print_param_list_node((param_list_node *)current_ast_node, space_size);
    break;
  case ast_node_func_return:
    print_func_return_node((func_return_node *)current_ast_node, space_size);
    break;
  case ast_node_body:
    print_body_node((body_node *)current_ast_node, space_size);
    break;
  case ast_node_def:
    print_def_node((def_node *)current_ast_node, space_size);
    break;
  case ast_node_def_list:
    print_def_list_node((def_list_node *)current_ast_node, space_size);
    break;
  case ast_node_equals:
    print_equals_node((equals_node *)current_ast_node, space_size);
    break;
  case ast_node_less_than:
    print_less_than_node((less_than_node *)current_ast_node, space_size);
    break;
  case ast_node_and:
    print_and_node((and_node *)current_ast_node, space_size);
    break;
  case ast_node_or:
    print_or_node((or_node *)current_ast_node, space_size);
    break;
  case ast_node_add:
    print_add_node((add_node *)current_ast_node, space_size);
    break;
  case ast_node_subtract:
    print_subtract_node((subtract_node *)current_ast_node, space_size);
    break;
  case ast_node_multiply:
    print_multiply_node((multiply_node *)current_ast_node, space_size);
    break;
  case ast_node_divide:
    print_divide_node((divide_node *)current_ast_node, space_size);
    break;
  case ast_node_not:
    print_not_node((not_node *)current_ast_node, space_size);
    break;
  case ast_node_negative:
    print_negative_node((negative_node *)current_ast_node, space_size);
    break;
  case ast_node_arg_list:
    print_arg_list_node((arg_list_node *)current_ast_node, space_size);
    break;
  case ast_node_func_call:
    print_func_call_node((func_call_node *)current_ast_node, space_size);
    break;
  case ast_node_if:
    print_if_node((if_node *)current_ast_node, space_size);
    break;
  case ast_node_int:
    print_int_node((int_node *)current_ast_node, space_size);
    break;
  case ast_node_bool:
    print_bool_node((bool_node *)current_ast_node, space_size);
    break;
  }
}

// FREE_AST

void free_name_node(name_node *ast_node) {
  if (ast_node->super.token) {
    free_token(ast_node->super.token);
    ast_node->super.token = NULL;
  }

  if (ast_node->value) {
    free(ast_node->value);
    ast_node->value = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_type_node(type_node *ast_node) {
  if (ast_node->super.token) {
    free_token(ast_node->super.token);
    ast_node->super.token = NULL;
  }

  if (ast_node->value) {
    free(ast_node->value);
    ast_node->value = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_id_node(id_node *ast_node) {
  if (ast_node->super.token) {
    free_token(ast_node->super.token);
    ast_node->super.token = NULL;
  }

  if (ast_node->value) {
    free(ast_node->value);
    ast_node->value = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_param_node(param_node *ast_node) {
  if (ast_node->super.token) {
    free_token(ast_node->super.token);
    ast_node->super.token = NULL;
  }

  if (ast_node->name) {
    free_name_node(ast_node->name);
    ast_node->name = NULL;
  }

  if (ast_node->param_type) {
    free_type_node(ast_node->param_type);
    ast_node->param_type = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_param_list_node(param_list_node *ast_node) {
  if (ast_node->super.token) {
    free_token(ast_node->super.token);
    ast_node->super.token = NULL;
  }

  for (int i = 0; i < ast_node->params_size; i++) {
    if (ast_node->params[i]) {
      free_param_node(ast_node->params[i]);
      ast_node->params[i] = NULL;
    }
  }

  if (ast_node->params) {
    free(ast_node->params);
    ast_node->params = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_func_return_node(func_return_node *ast_node) {
  if (ast_node->super.token) {
    free_token(ast_node->super.token);
    ast_node->super.token = NULL;
  }

  if (ast_node->return_type) {
    free_type_node(ast_node->return_type);
    ast_node->return_type = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_body_node(body_node *ast_node) {
  if (ast_node->super.token) {
    free_token(ast_node->super.token);
    ast_node->super.token = NULL;
  }

  for (int i = 0; i < ast_node->expressions_size; i++) {
    if (ast_node->expressions[i]) {
      free_ast(ast_node->expressions[i]);
      ast_node->expressions[i] = NULL;
    }
  }

  if (ast_node->expressions) {
    free(ast_node->expressions);
    ast_node->expressions = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_def_node(def_node *ast_node) {
  if (!ast_node) {
    return;
  }
  
  if (ast_node->super.token) {
    free_token(ast_node->super.token);
    ast_node->super.token = NULL;
  }

  if (ast_node->name) {
    free_name_node(ast_node->name);
    ast_node->name = NULL;
  }

  if (ast_node->param_list) {
    free_param_list_node(ast_node->param_list);
    ast_node->param_list = NULL;
  }

  if (ast_node->func_return) {
    free_func_return_node(ast_node->func_return);
    ast_node->func_return = NULL;
  }

  if (ast_node->body) {
    free_body_node(ast_node->body);
    ast_node->body = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_def_list_node(def_list_node *ast_node) {
  if (ast_node->super.token) {
    free_token(ast_node->super.token);
    ast_node->super.token = NULL;
  }

  for (int i = 0; i < ast_node->def_array_size; i++) {
    if (ast_node->def_array[i]) {
      free_def_node(ast_node->def_array[i]);
      ast_node->def_array[i] = NULL;
    }
  }

  if (ast_node->def_array) {
    free(ast_node->def_array);
    ast_node->def_array = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_equals_node(equals_node *ast_node) {
  if (ast_node->super.super.token) {
    free_token(ast_node->super.super.token);
    ast_node->super.super.token = NULL;
  }

  if (ast_node->super.left) {
    free_ast(ast_node->super.left);
    ast_node->super.left = NULL;
  }

  if (ast_node->super.right) {
    free_ast(ast_node->super.right);
    ast_node->super.right = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_less_than_node(less_than_node *ast_node) {
  if (ast_node->super.super.token) {
    free_token(ast_node->super.super.token);
    ast_node->super.super.token = NULL;
  }

  if (ast_node->super.left) {
    free_ast(ast_node->super.left);
    ast_node->super.left = NULL;
  }

  if (ast_node->super.right) {
    free_ast(ast_node->super.right);
    ast_node->super.right = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_and_node(and_node *ast_node) {
  if (ast_node->super.super.token) {
    free_token(ast_node->super.super.token);
    ast_node->super.super.token = NULL;
  }

  if (ast_node->super.left) {
    free_ast(ast_node->super.left);
    ast_node->super.left = NULL;
  }

  if (ast_node->super.right) {
    free_ast(ast_node->super.right);
    ast_node->super.right = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_or_node(or_node *ast_node) {
  if (ast_node->super.super.token) {
    free_token(ast_node->super.super.token);
    ast_node->super.super.token = NULL;
  }

  if (ast_node->super.left) {
    free_ast(ast_node->super.left);
    ast_node->super.left = NULL;
  }

  if (ast_node->super.right) {
    free_ast(ast_node->super.right);
    ast_node->super.right = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_add_node(add_node *ast_node) {
  if (ast_node->super.super.token) {
    free_token(ast_node->super.super.token);
    ast_node->super.super.token = NULL;
  }

  if (ast_node->super.left) {
    free_ast(ast_node->super.left);
    ast_node->super.left = NULL;
  }

  if (ast_node->super.right) {
    free_ast(ast_node->super.right);
    ast_node->super.right = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_subtract_node(subtract_node *ast_node) {
  if (ast_node->super.super.token) {
    free_token(ast_node->super.super.token);
    ast_node->super.super.token = NULL;
  }

  if (ast_node->super.left) {
    free_ast(ast_node->super.left);
    ast_node->super.left = NULL;
  }

  if (ast_node->super.right) {
    free_ast(ast_node->super.right);
    ast_node->super.right = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_multiply_node(multiply_node *ast_node) {
  if (ast_node->super.super.token) {
    free_token(ast_node->super.super.token);
    ast_node->super.super.token = NULL;
  }

  if (ast_node->super.left) {
    free_ast(ast_node->super.left);
    ast_node->super.left = NULL;
  }

  if (ast_node->super.right) {
    free_ast(ast_node->super.right);
    ast_node->super.right = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_divide_node(divide_node *ast_node) {
  if (ast_node->super.super.token) {
    free_token(ast_node->super.super.token);
    ast_node->super.super.token = NULL;
  }

  if (ast_node->super.left) {
    free_ast(ast_node->super.left);
    ast_node->super.left = NULL;
  }

  if (ast_node->super.right) {
    free_ast(ast_node->super.right);
    ast_node->super.right = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_not_node(not_node *ast_node) {
  if (ast_node->super.super.token) {
    free_token(ast_node->super.super.token);
    ast_node->super.super.token = NULL;
  }

  if (ast_node->super.next) {
    free_ast(ast_node->super.next);
    ast_node->super.next = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_negative_node(negative_node *ast_node) {
  if (ast_node->super.super.token) {
    free_token(ast_node->super.super.token);
    ast_node->super.super.token = NULL;
  }

  if (ast_node->super.next) {
    free_ast(ast_node->super.next);
    ast_node->super.next = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_arg_list_node(arg_list_node *ast_node) {
  if (ast_node->super.token) {
    free_token(ast_node->super.token);
    ast_node->super.token = NULL;
  }

  for (int i = 0; i < ast_node->args_size; i++) {
    if (ast_node->args[i]) {
      free_ast(ast_node->args[i]);
      ast_node->args[i] = NULL;
    }
  }

  if (ast_node->args) {
    free(ast_node->args);
    ast_node->args = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_func_call_node(func_call_node *ast_node) {
  if (ast_node->super.token) {
    free_token(ast_node->super.token);
    ast_node->super.token = NULL;
  }

  if (ast_node->name) {
    free_name_node(ast_node->name);
    ast_node->name = NULL;
  }

  if (ast_node->arg_list) {
    free_arg_list_node(ast_node->arg_list);
    ast_node->arg_list = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_if_node(if_node *ast_node) {
  if (ast_node->super.token) {
    free_token(ast_node->super.token);
    ast_node->super.token = NULL;
  }

  if (ast_node->if_exp) {
    free_ast(ast_node->if_exp);
    ast_node->if_exp = NULL;
  }

  if (ast_node->then_exp) {
    free_ast(ast_node->then_exp);
    ast_node->then_exp = NULL;
  }

  if (ast_node->else_exp) {
    free_ast(ast_node->else_exp);
    ast_node->else_exp = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_int_node(int_node *ast_node) {
  if (ast_node->super.token) {
    free_token(ast_node->super.token);
    ast_node->super.token = NULL;
  }

  if (ast_node->value) {
    free(ast_node->value);
    ast_node->value = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_bool_node(bool_node *ast_node) {
  if (ast_node->super.token) {
    free_token(ast_node->super.token);
    ast_node->super.token = NULL;
  }

  if (ast_node->value) {
    free(ast_node->value);
    ast_node->value = NULL;
  }

  free(ast_node);
  ast_node = NULL;
}

void free_ast(ast_node *current_ast_node) {
  int ast_type = current_ast_node->node_type;

  switch (ast_type) {
  case ast_node_name:
    free_name_node((name_node *)current_ast_node);
    break;
  case ast_node_id:
    free_id_node((id_node *)current_ast_node);
    break;
  case ast_node_param:
    free_param_node((param_node *)current_ast_node);
    break;
  case ast_node_param_list:
    free_param_list_node((param_list_node *)current_ast_node);
    break;
  case ast_node_func_return:
    free_func_return_node((func_return_node *)current_ast_node);
    break;
  case ast_node_body:
    free_body_node((body_node *)current_ast_node);
    break;
  case ast_node_def:
    free_def_node((def_node *)current_ast_node);
    break;
  case ast_node_def_list:
    free_def_list_node((def_list_node *)current_ast_node);
    break;
  case ast_node_equals:
    free_equals_node((equals_node *)current_ast_node);
    break;
  case ast_node_less_than:
    free_less_than_node((less_than_node *)current_ast_node);
    break;
  case ast_node_and:
    free_and_node((and_node *)current_ast_node);
    break;
  case ast_node_or:
    free_or_node((or_node *)current_ast_node);
    break;
  case ast_node_add:
    free_add_node((add_node *)current_ast_node);
    break;
  case ast_node_subtract:
    free_subtract_node((subtract_node *)current_ast_node);
    break;
  case ast_node_multiply:
    free_multiply_node((multiply_node *)current_ast_node);
    break;
  case ast_node_divide:
    free_divide_node((divide_node *)current_ast_node);
    break;
  case ast_node_not:
    free_not_node((not_node *)current_ast_node);
    break;
  case ast_node_negative:
    free_negative_node((negative_node *)current_ast_node);
    break;
  case ast_node_arg_list:
    free_arg_list_node((arg_list_node *)current_ast_node);
    break;
  case ast_node_func_call:
    free_func_call_node((func_call_node *)current_ast_node);
    break;
  case ast_node_if:
    free_if_node((if_node *)current_ast_node);
    break;
  case ast_node_int:
    free_int_node((int_node *)current_ast_node);
    break;
  case ast_node_bool:
    free_bool_node((bool_node *)current_ast_node);
    break;
  }
}
