#include <ast_nodes.h>
#include <parser.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char **argv) {
  if (argc == 1) {
    printf("Usage: %s <filename.twn>\n", argv[0]);
    exit(1);
  }

  FILE *input_program = fopen(argv[1], "r");

  ast_node *full_ast = NULL;

  full_ast = parse(input_program, (char *)argv[1]);
  fclose(input_program);

  if (!full_ast) {
    exit(1);
  }

  print_ast(full_ast, 0);

  free_ast(full_ast);
}
