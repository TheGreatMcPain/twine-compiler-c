#include <scanner.h>
#include <stdio.h>
#include <stdlib.h>

int main(const int argc, const char **argv) {
  if (argc == 1) {
    printf("Usage: %s <input file>\n", argv[0]);
  }

  const char *filename = argv[1];

  scanner_set_filename(filename);

  FILE *file = fopen(filename, "r");

  struct Token **tokens = NULL;

  int tokens_size = scan(file, &tokens);
  printf("Tokens: %d\n", tokens_size);

  for (int i = 0; i < tokens_size; i++) {
    print_token(tokens[i]);
  }

  // Free memory
  for (int i = 0; i < tokens_size; i++) {
    free_token(tokens[i]);
  }
  free(tokens);

  fclose(file);
}
