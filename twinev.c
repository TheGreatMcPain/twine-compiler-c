#include <stdlib.h>
#include <stdio.h>
#include <ast_nodes.h>
#include <symbol_table.h>
#include <semantic_analyzer.h>
#include <parser.h>

int main(int argc, const char **argv) {
  if (argc == 1) {
    printf("Usage: %s <filename.twn>\n", argv[0]);
    exit(1);
  }

  FILE *input_program = fopen(argv[1], "r");

  ast_node *full_ast = NULL;

  full_ast = parse(input_program, (char *)argv[1]);
  fclose(input_program);

  if (!full_ast) {
    exit(1);
  }

  printf("---\nABSTRACT_SYNTAX_TREE\n---\n");

  print_ast(full_ast, 0);

  printf("---\nSYMBOL_TABLE\n---\n\n");

  struct SymbolTable *symbol_table = NULL;

  symbol_table = analyze_semantics(full_ast);

  if (!symbol_table) {
    // semantic_analysis failed. Free AST
    free_ast(full_ast);
    exit(1);
  }

  // TODO: Print symbol table entries
  symbol_table_print(symbol_table);

  symbol_table_free(symbol_table);
  free_ast(full_ast);
}
