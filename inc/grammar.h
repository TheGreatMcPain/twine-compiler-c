#ifndef GRAMMAR_H
#define GRAMMAR_H
#include <stdbool.h>

void print_grammar_as_enums();

bool is_semantic_action(int input_value);

bool is_non_terminal(int input_value);

int get_grammar_as_enums(int grammar_as_enums[256][256]);

int grammar_item_to_enum(const char *grammar_item);

int grammar_item_to_term_token_type(const char *grammar_item);

int grammar_item_to_non_term(const char *grammar_item);

int grammar_item_to_semantic_action(const char *grammar_item);

#endif
