#ifndef FIRST_FOLLOW_SETS_H
#define FIRST_FOLLOW_SETS_H

void get_first_sets(int first_sets[256][256], int enum_grammar[256][256],
                    int enum_grammar_size);

void get_follow_sets(int follow_sets[256][256], int first_sets[256][256],
                     int enum_grammar[256][256], int enum_grammar_size);

void print_first_sets();

void print_follow_sets();

#endif
