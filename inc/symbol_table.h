#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H
#include <ast_nodes.h>
#include <stdbool.h>
#include <token_twine.h>

struct SymbolTableEntry {
  char *name;

  struct Token *token;

  // TODO: Create ast_type struct.
  int semantic_type;

  param_list_node *param_list;

  bool is_duplicate;
  bool is_analyzed;

  char **func_calls;
  int func_calls_size;
  char **calling_funcs;
  int calling_funcs_size;

  param_node **unused_params;
  int unused_params_size;
};

struct SymbolTableEntry *symbol_table_create_entry(def_node *ast_node);

bool symbol_table_entry_is_calling_func(
    struct SymbolTableEntry *sym_table_entry, char *func_name);

void symbol_table_entry_add_calling_func(
    struct SymbolTableEntry *sym_table_entry, char *func_name);

bool symbol_table_entry_is_func_call(struct SymbolTableEntry *sym_table_entry,
                                     char *func_name);

void symbol_table_entry_add_func_call(struct SymbolTableEntry *sym_table_entry,
                                      char *func_name);

param_node *
symbol_table_entry_get_param(struct SymbolTableEntry *sym_table_entry,
                             char *param_name);

bool symbol_table_entry_is_param(struct SymbolTableEntry *sym_table_entry,
                                 char *param_name);

bool symbol_table_entry_is_unused_param(
    struct SymbolTableEntry *sym_table_entry, char *param_name);

void symbol_table_entry_del_unused_param(
    struct SymbolTableEntry *sym_table_entry, char *param_name);

void symbol_table_entry_print(struct SymbolTableEntry *sym_table_entry);

void symbol_table_entry_free(struct SymbolTableEntry *sym_table_entry);

struct SymbolTable {
  struct SymbolTableEntry **entries;
  int entries_size;

  char **dup_func_names;
  int dup_func_names_size;
};

struct SymbolTable *create_symbol_table();

struct SymbolTableEntry *symbol_table_get_entry(struct SymbolTable *sym_table,
                                                char *func_name);

bool symbol_table_is_entry(struct SymbolTable *sym_table, char *func_name);

void symbol_table_add_entry(struct SymbolTable *sym_table, def_node *ast_node);

void symbol_table_print(struct SymbolTable *sym_table);

void symbol_table_free(struct SymbolTable *sym_table);

#endif
