#ifndef SEMANTIC_ANALYZER_H
#define SEMANTIC_ANALYZER_H
#include <ast_nodes.h>
#include <symbol_table.h>

struct SymbolTable *analyze_semantics(ast_node *ast);

#endif
