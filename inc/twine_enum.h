#ifndef TWINE_ENUM_H
#define TWINE_ENUM_H

// See: https://stackoverflow.com/a/10966395
#define FOREACH_STATE(STATE)                                                   \
  STATE(state_looking)                                                         \
  STATE(state_integer)                                                         \
  STATE(state_zero)                                                            \
  STATE(state_leftparen)                                                       \
  STATE(state_rightparen)                                                      \
  STATE(state_colon)                                                           \
  STATE(state_comma)                                                           \
  STATE(state_op_not)                                                          \
  STATE(state_op_and)                                                          \
  STATE(state_op_or)                                                           \
  STATE(state_op_lessthan)                                                     \
  STATE(state_op_divide)                                                       \
  STATE(state_op_multiply)                                                     \
  STATE(state_op_minus)                                                        \
  STATE(state_op_plus)                                                         \
  STATE(state_op_equals)                                                       \
  STATE(state_ids_and_reserved)

#define FOREACH_TOKENTYPE(TOKENTYPE)                                           \
  TOKENTYPE(token_type_int)                                                    \
  TOKENTYPE(token_type_bool)                                                   \
  TOKENTYPE(token_type_typename_boolean)                                       \
  TOKENTYPE(token_type_typename_integer)                                       \
  TOKENTYPE(token_type_punct_leftparen)                                        \
  TOKENTYPE(token_type_punct_rightparen)                                       \
  TOKENTYPE(token_type_punct_colon)                                            \
  TOKENTYPE(token_type_punct_comma)                                            \
  TOKENTYPE(token_type_op_not)                                                 \
  TOKENTYPE(token_type_op_and)                                                 \
  TOKENTYPE(token_type_op_or)                                                  \
  TOKENTYPE(token_type_op_lessthan)                                            \
  TOKENTYPE(token_type_op_divide)                                              \
  TOKENTYPE(token_type_op_multiply)                                            \
  TOKENTYPE(token_type_op_minus)                                               \
  TOKENTYPE(token_type_op_plus)                                                \
  TOKENTYPE(token_type_op_equals)                                              \
  TOKENTYPE(token_type_id)                                                     \
  TOKENTYPE(token_type_id_print)                                               \
  TOKENTYPE(token_type_keyword_f)                                              \
  TOKENTYPE(token_type_keyword_returns)                                        \
  TOKENTYPE(token_type_keyword_if)                                             \
  TOKENTYPE(token_type_keyword_else)                                           \
  TOKENTYPE(token_type_end_of_file)                                            \
  TOKENTYPE(token_type_epsilon)

#define FOREACH_SEMANTICACTION(ACTION)                                         \
  ACTION(semantic_action_make_def_list)                                        \
  ACTION(semantic_action_make_def)                                             \
  ACTION(semantic_action_make_id)                                              \
  ACTION(semantic_action_make_param_list)                                      \
  ACTION(semantic_action_make_func_return)                                     \
  ACTION(semantic_action_make_body)                                            \
  ACTION(semantic_action_make_param)                                           \
  ACTION(semantic_action_make_type)                                            \
  ACTION(semantic_action_make_equals)                                          \
  ACTION(semantic_action_make_less_than)                                       \
  ACTION(semantic_action_make_or)                                              \
  ACTION(semantic_action_make_add)                                             \
  ACTION(semantic_action_make_subtract)                                        \
  ACTION(semantic_action_make_and)                                             \
  ACTION(semantic_action_make_multiply)                                        \
  ACTION(semantic_action_make_divide)                                          \
  ACTION(semantic_action_make_not)                                             \
  ACTION(semantic_action_make_negative)                                        \
  ACTION(semantic_action_make_func_call)                                       \
  ACTION(semantic_action_make_if)                                              \
  ACTION(semantic_action_make_arg_list)                                        \
  ACTION(semantic_action_make_bool)                                            \
  ACTION(semantic_action_make_int)                                             \
  ACTION(semantic_action_make_name)

#define FOREACH_NONTERMINAL(NONTERM)                                           \
  NONTERM(nonterm_program)                                                     \
  NONTERM(nonterm_definition_list)                                             \
  NONTERM(nonterm_definition_list_)                                            \
  NONTERM(nonterm_definition)                                                  \
  NONTERM(nonterm_function_return)                                             \
  NONTERM(nonterm_body)                                                        \
  NONTERM(nonterm_print_expression)                                            \
  NONTERM(nonterm_parameter_list)                                              \
  NONTERM(nonterm_formal_parameters)                                           \
  NONTERM(nonterm_formal_parameters_)                                          \
  NONTERM(nonterm_id_with_type)                                                \
  NONTERM(nonterm_type)                                                        \
  NONTERM(nonterm_expression)                                                  \
  NONTERM(nonterm_expression_)                                                 \
  NONTERM(nonterm_simple_expression)                                           \
  NONTERM(nonterm_simple_expression_)                                          \
  NONTERM(nonterm_term)                                                        \
  NONTERM(nonterm_term_)                                                       \
  NONTERM(nonterm_factor)                                                      \
  NONTERM(nonterm_argument_list)                                               \
  NONTERM(nonterm_argument_list_)                                              \
  NONTERM(nonterm_formal_arguments)                                            \
  NONTERM(nonterm_formal_arguments_)                                           \
  NONTERM(nonterm_literal)

#define FOREACH_AST_NODE(NODE)                                                 \
  NODE(ast_node_def_list)                                                      \
  NODE(ast_node_def)                                                           \
  NODE(ast_node_id)                                                            \
  NODE(ast_node_param_list)                                                    \
  NODE(ast_node_func_return)                                                   \
  NODE(ast_node_body)                                                          \
  NODE(ast_node_param)                                                         \
  NODE(ast_node_type)                                                          \
  NODE(ast_node_equals)                                                        \
  NODE(ast_node_less_than)                                                     \
  NODE(ast_node_or)                                                            \
  NODE(ast_node_add)                                                           \
  NODE(ast_node_subtract)                                                      \
  NODE(ast_node_and)                                                           \
  NODE(ast_node_multiply)                                                      \
  NODE(ast_node_divide)                                                        \
  NODE(ast_node_not)                                                           \
  NODE(ast_node_negative)                                                      \
  NODE(ast_node_func_call)                                                     \
  NODE(ast_node_if)                                                            \
  NODE(ast_node_arg_list)                                                      \
  NODE(ast_node_bool)                                                          \
  NODE(ast_node_int)                                                           \
  NODE(ast_node_name)

#define FOREACH_SEMANTIC_TYPE(TYPE)                                            \
  TYPE(semantic_type_integer)                                                  \
  TYPE(semantic_type_boolean)                                                  \
  TYPE(semantic_type_function)                                                 \
  TYPE(semantic_type_none)                                                     \
  TYPE(semantic_type_error)

#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,

enum TwineEnum {
  FOREACH_STATE(GENERATE_ENUM) FOREACH_TOKENTYPE(GENERATE_ENUM)
      FOREACH_SEMANTICACTION(GENERATE_ENUM) FOREACH_NONTERMINAL(GENERATE_ENUM)
          FOREACH_AST_NODE(GENERATE_ENUM) FOREACH_SEMANTIC_TYPE(GENERATE_ENUM)
};

extern const char *TwineEnumStrings[];

#endif
