#ifndef SCANNER_UTILS_H
#define SCANNER_UTILS_H
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

// Check if a character is a punctuation
bool is_punct(char input);

// Get token type for punctuation
int get_punct_token_type(char input);

// Get token type for punctuation
int get_punct_state(char input);

bool is_punct_state(int state);

// Check if a character is an operation
bool is_operation(char input);

int get_operation_token_type(char input);

int get_operation_state(char input);

bool is_operation_state(int state);

// Helper function which checks
// is_operation, is_punct, or if the input file is EOF.
bool is_terminate(char input_char, FILE *input_file_stream);

// Walk filepointer until newline character is reached.
char move_to_newline(FILE *file_stream);

#endif
