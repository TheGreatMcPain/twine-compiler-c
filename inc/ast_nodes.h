#ifndef AST_NODES_H
#define AST_NODES_H
// AST_NODE structs (with "inheritance")
// Some usage examples...
//
// def_node->super.node_type;
//
// Would be the same as...
//
// ast_node *casted = (ast_node *)def_node;
// casted->node_type;

typedef struct {
  int node_type;
  int semantic_type;
  struct Token *token;
} ast_node;

typedef struct {
  ast_node super;

  char *value;
} name_node;

typedef struct {
  ast_node super;

  char *value;
} id_node;

typedef struct {
  ast_node super;

  char *value;
} type_node;

typedef struct {
  ast_node super;

  name_node *name;
  type_node *param_type;
} param_node;

typedef struct {
  ast_node super;

  param_node **params;
  int params_size;
} param_list_node;

typedef struct {
  ast_node super;

  type_node *return_type;
} func_return_node;

typedef struct {
  ast_node super;

  ast_node **expressions;
  int expressions_size;
} body_node;

typedef struct {
  ast_node super;

  name_node *name;
  param_list_node *param_list;
  func_return_node *func_return;
  body_node *body;
} def_node;

typedef struct {
  ast_node super;

  def_node **def_array;
  int def_array_size;

} def_list_node;

typedef struct {
  ast_node super;

  ast_node *left;
  ast_node *right;
} binary_exp;

typedef struct {
  ast_node super;

  ast_node *next;
} unary_exp;

typedef struct {
  binary_exp super;
} equals_node;

typedef struct {
  binary_exp super;
} less_than_node;

typedef struct {
  binary_exp super;
} and_node;

typedef struct {
  binary_exp super;
} or_node;

typedef struct {
  binary_exp super;
} add_node;

typedef struct {
  binary_exp super;
} subtract_node;

typedef struct {
  binary_exp super;
} multiply_node;

typedef struct {
  binary_exp super;
} divide_node;

typedef struct {
  unary_exp super;
} not_node;

typedef struct {
  unary_exp super;
} negative_node;

typedef struct {
  ast_node super;

  ast_node **args;
  int args_size;
} arg_list_node;

typedef struct {
  ast_node super;

  name_node *name;
  arg_list_node *arg_list;
} func_call_node;

typedef struct {
  ast_node super;

  ast_node *if_exp;
  ast_node *then_exp;
  ast_node *else_exp;
} if_node;

typedef struct {
  ast_node super;

  char *value;
} int_node;

typedef struct {
  ast_node super;

  char *value;
} bool_node;

void print_ast(ast_node *current_ast_node, int space_size);

void free_ast(ast_node *ast_node);

#endif
