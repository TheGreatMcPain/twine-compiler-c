#ifndef CODE_GEN_UTILS_H
#define CODE_GEN_UTILS_H

// data structures used to represent the intermediate code
#define FOREACH_INTER(INSTRUCTION) \
  INSTRUCTION(inter_instruction_add) \
  INSTRUCTION(inter_instruction_subtract) \
  INSTRUCTION(inter_instruction_multiply) \
  INSTRUCTION(inter_instruction_divide) \
  INSTRUCTION(inter_instruction_or) \
  INSTRUCTION(inter_instruction_equals) \
  INSTRUCTION(inter_instruction_and) \
  INSTRUCTION(inter_instruction_lessthan)   \
  INSTRUCTION(inter_instruction_param) \
  INSTRUCTION(inter_instruction_out) \
  INSTRUCTION(inter_instruction_funccall) \
  INSTRUCTION(inter_instruction_ifthen) \
  INSTRUCTION(inter_instruction_else) \
  INSTRUCTION(inter_instruction_endif) \
  INSTRUCTION(inter_instruction_not) \
  INSTRUCTION(inter_instruction_negative) \
  INSTRUCTION(inter_instruction_boolean) \
  INSTRUCTION(inter_instruction_integer) \
  INSTRUCTION(inter_instruction_return) \
  INSTRUCTION(inter_instruction_arg) \
  INSTRUCTION(inter_instruction_storetemp) \
  INSTRUCTION(inter_instruction_loadtemp) \
  INSTRUCTION(inter_instruction_setupdmem) \

#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,

enum InterInstEnum {
  GENERATE_ENUM(FOREACH_INTER)
};
  
extern char *InterInstString[];

typedef struct {
  int instruction_type;
  int left;
  int right;
  int result;
} inter_instruction;

#endif
