#ifndef SEMANTIC_ACTIONS_H
#define SEMANTIC_ACTIONS_H
#include <ast_nodes.h>

// AST NODE STACK

struct ast_node_stack {
  ast_node **array;
  int array_size;
};

void ast_node_stack_init(struct ast_node_stack *input_stack);

void ast_node_stack_free(struct ast_node_stack *input_stack);

ast_node *ast_node_stack_top(struct ast_node_stack *stack);

ast_node *ast_node_stack_pop(struct ast_node_stack *stack);

void ast_node_stack_push(struct ast_node_stack *stack, ast_node *value);

// INT STACK

struct int_stack {
  int *array;
  int array_size;
};

void int_stack_init(struct int_stack *input_stack);

void int_stack_free(struct int_stack *input_stack);

int int_stack_top(struct int_stack *stack);

int int_stack_pop(struct int_stack *stack);

void int_stack_push(struct int_stack *stack, int value);

// SEMANTIC ACTIONS

void make_def_list(struct ast_node_stack *semantic_stack);

void make_def(struct ast_node_stack *semantic_stack);

void make_id(struct Token *matched_terminal,
             struct ast_node_stack *semantic_stack);

void make_param_list(struct ast_node_stack *semantic_stack);

void make_func_return(struct ast_node_stack *semantic_stack);

void make_body(struct ast_node_stack *semantic_stack);

void make_param(struct ast_node_stack *semantic_stack);

void make_type(struct Token *matched_terminal,
               struct ast_node_stack *semantic_stack);

void make_equals(struct ast_node_stack *semantic_stack);

void make_less_then(struct ast_node_stack *semantic_stack);

void make_or(struct ast_node_stack *semantic_stack);

void make_and(struct ast_node_stack *semantic_stack);

void make_add(struct ast_node_stack *semantic_stack);

void make_subtract(struct ast_node_stack *semantic_stack);

void make_multiply(struct ast_node_stack *semantic_stack);

void make_divide(struct ast_node_stack *semantic_stack);

void make_not(struct ast_node_stack *semantic_stack);

void make_negative(struct ast_node_stack *semantic_stack);

void make_int(struct Token *matched_terminal,
              struct ast_node_stack *semantic_stack);

void make_bool(struct Token *matched_terminal,
               struct ast_node_stack *semantic_stack);

void make_name(struct Token *matched_terminal,
               struct ast_node_stack *semantic_stack);

void matched_term_action(int semantic_action, struct Token *matched_terminal,
                         struct ast_node_stack *semantic_stack);

void action(int semantic_action, struct ast_node_stack *semantic_stack);

#endif
