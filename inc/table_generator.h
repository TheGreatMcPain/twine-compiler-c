#ifndef TABLE_GENERATOR_H
#define TABLE_GENERATOR_H

void gen_parse_table(int *parse_table[256][256]);

void print_parse_table();

void free_parse_table(int *parse_table[256][256]);

#endif
