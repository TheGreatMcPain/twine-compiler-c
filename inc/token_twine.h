#ifndef TOKEN_TWINE_H
#define TOKEN_TWINE_H

struct Token {
  int token_type;
  char *value;
  int line_no;
  int char_no;
  char *current_line;
  char *filename;
};

struct Token *create_token(int token_type, char *value, int line_no,
                           int char_no, char *current_line);

void set_token_filename(struct Token *token, char *filename);

// Create a new token copy of 'input_token'.
struct Token *copy_token(struct Token *input_token);

void free_token(struct Token *token_pointer);

void print_token(struct Token *token);

#endif
