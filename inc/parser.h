#ifndef PARSER_H
#define PARSER_H
#include <ast_nodes.h>
#include <stdio.h>

ast_node *parse(FILE *input_program, char *filename);

#endif
