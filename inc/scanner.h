#ifndef SCANNER_H
#define SCANNER_H

#include <stdbool.h>
#include <stdio.h>
#include <token_twine.h>

void scanner_set_filename(const char *filename);

// Check if a character is a punctuation
bool is_punct(char input);

// Get token type for punctuation
int get_punct_token_type(char input);

// Get token type for punctuation
int get_punct_state(char input);

// Check if a character is an operation
bool is_operation(char input);

int get_operation_token_type(char input);

int get_operation_state(char input);

int scan(FILE *input_program, struct Token ***tokens);

// Get next token without advancing pointer.
struct Token *peek(FILE *input_program);

// Get next token and advance pointer.
struct Token *next(FILE *input_program);

void get_current_line(FILE *file_stream, char current_line[]);

#endif
