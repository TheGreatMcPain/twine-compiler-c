#ifndef TWINE_ERRORS_H
#define TWINE_ERRORS_H
#include <token_twine.h>

void create_error_message(const char *filename, int line_no, int char_no,
                          const char *line_contents, char message[]);

// Scanner error = LexicalError
void raise_LexicalError(const char *filename, int line_no, int char_no,
                        const char *line_contents, int scanner_state,
                        const char *error_msg);

void raise_SyntacticError(struct Token *token, const char *error_msg);

void raise_EofError(const char *error_msg);

void raise_SemanticError(const char *error_msg);

#endif
